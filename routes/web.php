<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', 'StudentController@index');
Route::get('/', function () {
    return view('welcome');
});

// Route::get('/student', function () {
//     return view('student.index');
// });
Route::get('/parent', function () {
    return view('parent.index');
});
Route::get('/subject', function () {
    return view('subject.index');
});
Route::get('/examTable', function () {
    return view('examTable.index');
});
Route::get('/timeTable', function () {
    return view('timeTable.index');
});
Route::get('/checkName', function () {
    return view('checkName.index');
});
Route::get('/information', function () {
    return view('information.index');
});

// Auth::routes();
Route::get('/login/{role}', 'Auth\LoginController@login')->name('login');
Route::get('/login', 'HomeController@index')->name('home');
// Route::get('/login/{role}', 'Auth\LoginController@login')->name('login');
Route::post('login', 'Auth\LoginController@authenticate');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('auth/register', [
  'as' => 'register', 
  'uses' => 'Auth\RegisterController@register'
]);
// Route::post('register', 'Auth\RegisterController@storeUser');
Route::post('auth/register', [
    'as' => 'register', 
    'uses' => 'Auth\RegisterController@storeUser'
  ]);

Route::resource('/students', 'StudentController');
Route::resource('/rooms', 'RoomController');
Route::resource('/parents', 'ParentStudentController');
Route::resource('/subjects', 'SubjectController');
Route::resource('/classTimes', 'ClassTimeController');
Route::resource('/timetables', 'TimetableController');
Route::get('/timetables/room/{room_id}', 'TimetableController@room');
Route::get('/timetables/create/{room_id}', 'TimetableController@create');
// Route::get('/timetables', 'TimeTableController@index');
Route::resource('/exams', 'ExamController');
Route::resource('/schoolRecords', 'SchoolRecordController');
Route::get('/schoolRecords/insertDetail/{student_id}', 'SchoolRecordController@insertDetail');
Route::resource('/classLevels', 'ClassLevelController');
Route::resource('/informations', 'InformationController');
Route::get('/checkNames/history', 'CheckNameController@history');
Route::resource('/checkNames', 'CheckNameController');
Route::get('/checkNames/selectTimetable/{room_id}', 'CheckNameController@selectTimetable');
Route::get('/checkNames/checking/room/{room_id}/timetable/{timetable_id}', 'CheckNameController@checking');
Route::get('/checkLineUps/history', 'CheckLineUpController@history');
Route::resource('/checkLineUps', 'CheckLineUpController');
Route::get('/checkLineUps/checking/room/{room_id}', 'CheckLineUpController@checking');

// Route::get('/checkNames/history', 'CheckNameController@history')->name('checkNames.history');
Route::resource('/teachers', 'TeacherController');
Route::resource('/messages', 'MessageController');
Route::get('/home', 'HomeController@index')->name('home');

Route::resource('/visitHomes', 'VisitHomeController');
