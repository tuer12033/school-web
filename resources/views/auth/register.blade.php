@extends('layout.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">ลงทะเบียน</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('ชื่อที่แสดง') }}<span style="color:red">*</span></label>

                            <div class="col-md-6">
                                <input maxlength="50" id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                    name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                                <label for="code" class="col-md-4 col-form-label text-md-right">{{ __('รหัส') }}<span style="color:red">*</span></label>
    
                                <div class="col-md-6">
                                    <input maxlength="50" id="code" type="text" class="form-control"
                                        name="code" required>
                                </div>
                            </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('ชื่อ') }}<span style="color:red">*</span></label>

                            <div class="col-md-6">
                                <input maxlength="50" id="firstName" type="text" class="form-control"
                                    name="firstName" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="lastName" class="col-md-4 col-form-label text-md-right">{{ __('นามสกุล') }}<span style="color:red">*</span></label>

                            <div class="col-md-6">
                                <input maxlength="50" id="lastName" type="text" class="form-control"
                                    name="lastName" required>

                            
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('ที่อยู่') }}<span style="color:red">*</span></label>

                            <div class="col-md-6">
                                <input id="address" type="text" class="form-control"
                                    name="address" required>

                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('เบอร์ติดต่อ') }}<span style="color:red">*</span></label>

                            <div class="col-md-6">
                                <input maxlength="20" id="tel" type="number" class="form-control"
                                    name="tel" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('ตำแหน่ง') }}<span style="color:red">*</span></label>

                            <div class="col-md-6">
                                {{-- <input id="position" type="text" class="form-control"
                                    name="position" required> --}}
                                    <select class="form-control form-control-user" name="position">
                                            <option value='Teacher'>อาจารย์</option>
                                            <option value='Boss'>ผู้อำนวยการ</option>
                                            <option value='Admin'>แอดมิน</option>
                                        </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email"
                                class="col-md-4 col-form-label text-md-right">{{ __('อีเมล') }}<span style="color:red">*</span></label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                    name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password"
                                class="col-md-4 col-form-label text-md-right">{{ __('รหัสผ่าน') }}<span style="color:red">*</span></label>

                            <div class="col-md-6">
                                <input id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password"
                                    required autocomplete="new-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm"
                                class="col-md-4 col-form-label text-md-right">{{ __('ยืนยันรหัสผ่าน') }}<span style="color:red">*</span></label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control"
                                    name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('ลงทะเบียน') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection