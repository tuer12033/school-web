@extends('layout.app')
@section('content')
<div>
    <h1>เพิ่มข้อมูลห้องเรียน<h1>
    {!! Form::open(['action' => 'RoomController@store', 'method' => 'POST']) !!}
    <div >
        {{Form::label('roomName','ห้องเรียน')}}
        {{Form::text('roomName','',['class'=> 'form-control', 'placeholder'=> 'ห้องเรียน', 'maxlength'=>"50"])}}
    </div>
    {{Form::submit('บันทึก',['class'=>'btn btn-primary'])}}
    {!! Form::close() !!}

</div>

@endsection