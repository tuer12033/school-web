@extends('layout.app')
@section('content')
<div class="container-fluid">
    <!-- DataTales Example -->

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">ประวัติเช็คชื่อ</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>รหัส</th>
                            <th>ชื่อ-นามสกุล</th>
                            <th>ห้อง</th>
                            <th>วัน</th>
                            <th>เวลา</th>
                            <th>วันที่</th>
                            <th>สถานะ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($checkNames as $checkName)

                        <tr>
                            <td>{{$checkName->student->code}}</td>
                            <td>{{$checkName->student->firstName}} {{$checkName->student->lastName}}</td>
                            <td>{{$checkName->student->room->roomName}}</td>
                            <td>{{$checkName->timetable->classTime->date}}</td>
                            <td>{{$checkName->timetable->classTime->start}} - {{$checkName->timetable->classTime->end}}</td>
                            <td>{{$checkName->created_at}}</td>
                            <td>{{$checkName->status}}</td>
                        </tr>

                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

@endsection