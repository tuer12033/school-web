@extends('layout.app')

@section('content')
<div class="container-fluid">
    <!-- DataTales Example -->

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">เลือกห้องเรียน</h6>
        </div>
        <a href="/checkNames/history" class="btn btn-primary">ดูประวัติ</a>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>ห้องเรียน</th>

                            <th>เลือกห้องเรียน</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($rooms as $room)

                        <tr>
                            <td>{{$room->roomName}}</td>
                            <td><a href="/checkNames/selectTimetable/{{$room->id}}"
                                    class="btn btn-secondary btn-icon-split">
                                    <span class="text">เลือกวิชาเช็คชื่อ</span>
                                </a>
                            </td>

                        </tr>

                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

@endsection