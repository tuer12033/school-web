@extends('layout.app')

@section('content')
<div class="container-fluid">
    <!-- DataTales Example -->

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">เลือกวิชาเช็คชื่อ</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>วิชา</th>
                            <th>เช็คชื่อนักเรียน</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($timetables as $timetable)

                        <tr>
                            <td>{{$timetable->subject->name}}</td>
                            <td><a href="/checkNames/checking/room/{{$room_id}}/timetable/{{$timetable->id}}"
                                    class="btn btn-secondary btn-icon-split">
                                    <span class="text">เช็คชื่อนักเรียน</span>
                                </a>
                            </td>

                        </tr>

                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

@endsection