@extends('layout.app')

@section('content')
<div>
    <h1>ข้อมูลห้องเรียน<h1>
            <a href="/rooms/create" class="btn btn-primary">เพื่มข้อมูล</a>
            <a href="/checkNames/history" class="btn btn-primary">ประวิติ</a>
            
            @if(count($rooms) > 0)
            <ul class="list-group">
                @foreach($rooms as $room)
                <li class="list-group-item">
                    <a href="/rooms/{{$room->id}}">{{$room->roomName}}</a>
                </li>
                @endforeach
            </ul>
            @else
            <h1>ไม่มีข้อมูลห้องเรียน<h1>
                    @endif
</div>

@endsection