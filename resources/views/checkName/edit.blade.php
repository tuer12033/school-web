@extends('layout.app')

@section('content')
<div>
    <h1>แก้ไขข้อมูลห้องเรียน</h1>
    {!! Form::open(['action' => ['RoomController@update', $room->id], 'method' => 'POST']) !!}
    <div class="form-group">
        {{Form::label('roomName','ชื่อห้อง')}}
        {{Form::text('roomName',$room->roomName,['class'=> 'form-control', 'placeholder'=> 'ชื่อห้อง', 'maxlength'=>"50"])}}
    </div>
    {{Form::hidden('_method','PUT')}}
    {{Form::submit('บันทึก',['class'=>'btn btn-primary'])}}
    {!! Form::close() !!}

</div>

@endsection