@extends('layout.app')

@section('content')
<div>
    <h1>ห้อง : {{$room->roomName}}</h1>
    <h2>รายชื่อนักเรียน</h2>
    <a href="/rooms/{{$room->id}}/edit" class="btn btn-default">แก้ไข</a>
    {!! Form::open(['action' => ['RoomController@destroy', $room->id], 'method' => 'POST', 'class'=>'pull-right'])
    !!}
    {{Form::hidden('_method','DELETE')}}
    {{Form::submit('ลบ',['class'=>'btn btn-danger'])}}
    {!! Form::close() !!}
    <h1>รายชื่อนักเรียน<h1>
    @foreach($students as $student)
    <li class="list-group-item">
        <a href="/students/{{$student->id}}">{{$student->firstName}} {{$student->lastName}}</a>
    </li>
    @endforeach
    
</div>

@endsection