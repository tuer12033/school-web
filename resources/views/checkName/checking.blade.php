@extends('layout.app')
@section('content')
<div>
    <h1>เช็คชื่อนักเรียน</h1>
    @if(count($students) > 0)
    {!! Form::open(['action' => 'CheckNameController@store', 'method' => 'POST']) !!}

    {{-- <label class="checkbox-inline">
        <input type="checkbox" id="check_list" name="check_list[]" value={{$student->id}}>
    {{$student->firstName}} {{$student->lastName}}
    </label> --}}
    <table class="table table-bordered">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">ชื่อ-นามสกุล</th>
                <th scope="col">เช็คชื่อ</th>
            </tr>
        </thead>
        <tbody>
            @foreach($students as $indexKey => $student)
            <tr>
                <td>
                    {{++$indexKey}}
                </td>
                <td>{{$student->firstName}} {{$student->lastName}}</td>
                <td>
                    <div class="form-check form-check-inline">
                        <input maxlength="50" class="form-check-input" type="radio" name="check_list[{{$student->id}}]"
                            value="present">
                        <label class="form-check-label">มา</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="check_list[{{$student->id}}]" value="absent" checked>
                        <label class="form-check-label">ขาด</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="check_list[{{$student->id}}]" value="late">
                        <label class="form-check-label">สาย</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="check_list[{{$student->id}}]" value="leave">
                        <label class="form-check-label">ลา</label>
                    </div>
                </td>
            </tr>
            <input name="date" type="hidden" value={{Carbon\Carbon::today()->format('Y-m-d')}}>
            <input name="student_list[]" type="hidden" value={{$student->id}}>
            <input name="timetable_id" type="hidden" value={{$timetable_id}}>
            @endforeach

        </tbody>
    </table>


    {{Form::submit('บันทึก',['class'=>'btn btn-primary'])}}
    {!! Form::close() !!}
    @else
    <h1>ไม่มีข้อมูลนักเรียน</h1>
    @endif

</div>

@endsection