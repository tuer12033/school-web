@extends('layout.app')

@section('content')

<div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
        <div class="row">
            <div class="col-lg">
                <div class="p-5">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">ส่งข้อความ<span style="color:red">*</span></h1>
                    </div>
                    {!! Form::open(['action' => 'MessageController@store', 'method' => 'POST']) !!}
                    
                    {{-- <div class="form-group row">

                        <div class="col-sm-6 mb-3 mb-sm-0">
                            {{Form::text('firstName','',['class'=> 'form-control form-control-user', 'placeholder'=> 'ชื่อ'])}}
                        </div>
                        <div class="col-sm-6">
                            {{Form::text('lastName','',['class'=> 'form-control form-control-user', 'placeholder'=> 'นามสกุล'])}}

                        </div>
                    </div> --}}
                    <div class="form-group">
                        <h1 class="h6 text-gray-900 mb-4">เลือกนักเรียน<span style="color:red">*</span></h1>
                        <select class="custom-select custom-select-sm" name="receiver_id">
                            @foreach($students as $student)
                            <option value={{$student->id}}>{{$student->firstName}} {{$student->lasttName}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <h1 class="h6 text-gray-900 mb-4">ข้อความ<span style="color:red">*</span></h1>
                        {{Form::text('message','',['class'=> 'form-control form-control-user', 'placeholder'=> 'ข้อความ', 'maxlength'=>"50"])}}
                    </div>
                    {{Form::submit('บันทึก',['class'=>'btn btn-primary btn-user btn-block'])}}
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>

@endsection