@extends('layout.app')

@section('content')

<div class="container-fluid">
    <!-- DataTales Example -->

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">ข้อมูลจดหมาย</h6>
        </div>
        <a href="/messages/create" class="btn btn-primary">ส่งข้อความ</a>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>จาก</th>
                            <th>ถึง</th>
                            <th>ข้อความ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($messages as $message)
                        <tr>
                            <td>{{$message->sender_id}}</td>
                            <td>{{$message->receiver_id}}</td>
                            <td>{{$message->message}}</td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

@endsection