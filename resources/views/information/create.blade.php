@extends('layout.app')

@section('content')

<div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
        <div class="row">
            <div class="col-lg">
                <div class="p-5">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">เพิ่มข้อมูลประชาสัมพันธ์</h1>
                    </div>
                    {!! Form::open(['action' => 'InformationController@store', 'method' => 'POST']) !!}
                    <div class="form-group">
                        {{Form::label('title','หัวข้อ')}}<span style="color:red">*</span>
                        {{Form::text('title','',['class'=> 'form-control form-control-user', 'placeholder'=> 'หัวข้อ', 'maxlength'=>"50"])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('detail','รายละเอียด')}}<span style="color:red">*</span>
                        {{Form::text('detail','',['class'=> 'form-control form-control-user', 'placeholder'=> 'รายละเอียด', 'maxlength'=>"50"])}}
                    </div>
                    {{Form::submit('บันทึก',['class'=>'btn btn-primary btn-user btn-block'])}}
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>

@endsection