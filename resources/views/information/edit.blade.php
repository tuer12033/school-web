@extends('layout.app')

@section('content')
<div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
        <div class="row">
            <div class="col-lg">
                <div class="p-5">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">แก้ไขข้อมูลประชาสัมพันธ์</h1>
                    </div>
                    {!! Form::open(['action' => ['InformationController@update', $information->id], 'method' => 'POST'])
                    !!}

                    <div class="form-group">
                        {{Form::label('title','หัวข้อ')}}
                        {{Form::text('title',$information->title,['class'=> 'form-control form-control-user', 'placeholder'=> 'หัวข้อ', 'maxlength'=>"50"])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('detail','รายละเอียด')}}
                        {{Form::text('detail',$information->detail,['class'=> 'form-control form-control-user', 'placeholder'=> 'รายละเอียด', 'maxlength'=>"50"])}}
                    </div>
                    {{Form::hidden('_method','PUT')}}
                    {{Form::submit('บันทึก',['class'=>'btn btn-primary btn-user btn-block'])}}
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>

@endsection