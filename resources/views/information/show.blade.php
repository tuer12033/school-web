@extends('layout.app')

@section('content')
<div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
            <div class="row">
                <div class="col-lg">
                    <div class="p-5">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">ข้อมูลประชาสัมพันธ์</h1>
                        </div>
                        <div class="form-group">
                                <h5>หัวข้อ<h5>
                            {{Form::text('subjectCode',$information->title,['class'=> 'form-control form-control-user', 'readonly', 'placeholder'=> 'รหัสวิชา', 'maxlength'=>"50"])}}
                        </div>
                        <div class="form-group">
                                <h5>รายละเอียด<h5>
                            {{Form::text('name',$information->detail,['class'=> 'form-control form-control-user', 'readonly', 'placeholder'=> 'ชื่อวิชา', 'maxlength'=>"50"])}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection