@extends('layout.app')
@section('content')
<div class="container-fluid">
    <!-- DataTales Example -->

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">ประวัติเช็คชื่อเข้าแถว</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>รหัส</th>
                            <th>ชื่อ-นามสกุล</th>
                            <th>ห้อง</th>
                            <th>วันที่</th>
                            <th>สถานะ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($checkLineUps as $checkLineUp)

                        <tr>
                            <td>{{$checkLineUp->student->code}}</td>
                            <td>{{$checkLineUp->student->firstName}} {{$checkLineUp->student->lastName}}</td>
                            <td>{{$checkLineUp->student->room->roomName}}</td>
                            <td>{{$checkLineUp->date}}</td>
                            <td>{{$checkLineUp->status}}</td>
                        </tr>

                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

@endsection