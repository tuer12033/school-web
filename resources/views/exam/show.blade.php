@extends('layout.app')

@section('content')
<div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
        <div class="row">
            <div class="col-lg">
                <div class="p-5">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">ข้อมูลตารางสอบ</h1>
                    </div>
                    <div class="form-group">
                        <h1 class="h6 text-gray-900 mb-4">ระดับชั้น</h1>
                        {{Form::text('classLevel',$exam->classLevel->name,['class'=> 'form-control form-control-user', 'readonly', 'placeholder'=> 'ระดับชั้น', 'maxlength'=>"50"])}}
                    </div>
                    <div class="form-group">
                        <h1 class="h6 text-gray-900 mb-4">วิชา</h1>
                        {{Form::text('subject',$exam->subject->name,['class'=> 'form-control form-control-user', 'readonly', 'placeholder'=> 'วิชา', 'maxlength'=>"50"])}}
                    </div>
                    <div class="form-group">
                        <h1 class="h6 text-gray-900 mb-4">วันที่</h1>
                        {{Form::text('date',$exam->date,['class'=> 'form-control form-control-user', 'readonly', 'placeholder'=> 'วันที่'])}}
                    </div>
                    <div class="form-group">
                        <h1 class="h6 text-gray-900 mb-4">เวลา</h1>
                        {{Form::text('time',$exam->start.' '.$exam->end,['class'=> 'form-control form-control-user', 'readonly', 'placeholder'=> 'เวลา'])}}
                    </div>
                    <div class="form-group">
                        <h1 class="h6 text-gray-900 mb-4">ผู้คุมสอบ</h1>
                        {{Form::text('teacher',$exam->teacher->name,['class'=> 'form-control form-control-user', 'readonly', 'placeholder'=> 'ผู้คุมสอบ', 'maxlength'=>"50"])}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@endsection