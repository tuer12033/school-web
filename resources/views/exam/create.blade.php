@extends('layout.app')

@section('content')

<div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
        <div class="row">
            <div class="col-lg">
                <div class="p-5">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">เพิ่มข้อมูลวิชาสอบ</h1>
                    </div>

                    {!! Form::open(['action' => 'ExamController@store', 'method' => 'POST']) !!}
                    <div class="form-group">
                        {{Form::label('classLevel_id','ระดับชั้น')}}<span style="color:red">*</span>
                        <select class="form-control form-control-user" name="classLevel_id">
                            @foreach($classLevels as $classLevel)
                            <option value={{$classLevel->id}}>{{$classLevel->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        {{Form::label('subject_id','วิชา')}}<span style="color:red">*</span>
                        <select class="form-control form-control-user" name="subject_id">
                            @foreach($subjects as $subject)
                            <option value={{$subject->id}}>{{$subject->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        {{Form::label('date','วันที่')}}<span style="color:red">*</span>
                        {{ Form::date('date', new DateTime() ,['class'=> 'form-control form-control-user', 'placeholder'=> 'วันที่', 'maxlength'=>"50"]) }}
                    </div>
                    <div class="form-group">
                        {{Form::label('start','เวลาเริ่มวิชา')}}<span style="color:red">*</span>
                        {{ Form::time('start', '00:00',['class'=> 'form-control form-control-user', 'placeholder'=> 'นามสกุล']) }}
                    </div>
                    <div class="form-group">
                        {{Form::label('end','เวลาจบวิชา')}}<span style="color:red">*</span>
                        {{ Form::time('end', '00:00',['class'=> 'form-control form-control-user', 'placeholder'=> 'นามสกุล']) }}
                    </div>
                    <div class="form-group">
                        {{Form::label('teacher_id','ผู้คุมสอบ')}}<span style="color:red">*</span>
                        <select class="form-control form-control-user" name="teacher_id">
                            @foreach($teachers as $teacher)
                            <option value={{$teacher->id}}>{{$teacher->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        {{Form::label('type','ภาคเรียน')}}<span style="color:red">*</span>
                        <select class="form-control form-control-user" name="type">
                            <option value='Mid'>กลางภาค</option>
                            <option value='End'>ปลายภาค</option>
                        </select>
                    </div>
                    {{Form::submit('บันทึก',['class'=>'btn btn-primary btn-user btn-block'])}}
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>

@endsection