@extends('layout.app')

@section('content')

<div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
        <div class="row">
            <div class="col-lg">
                <div class="p-5">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">แก้ไขวิชาสอบ</h1>
                    </div>
                    {!! Form::open(['action' => ['ExamController@update', $exam->id], 'method' => 'POST']) !!}
                    <div class="form-group">
                        {{Form::label('class_id','ระดับชั้น')}}
                        <select class="form-control" name="classLevel_id">
                            @foreach ($classLevels as $classLevel)
                            <option value="{{ $classLevel->id }}" @if ($classLevel->id == old('classLevel_id',
                                $exam->classLevel_id))
                                selected="selected"
                                @endif
                                >{{ $classLevel->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        {{Form::label('subject_id','วิชา')}}
                        <select class="form-control" name="subject_id">
                            @foreach ($subjects as $subject)
                            <option value="{{ $subject->id }}" @if ($subject->id == old('subject_id',
                                $exam->subject_id))
                                selected="selected"
                                @endif
                                >{{ $subject->name }} </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        {{Form::label('date','วัน')}}
                        {!! Form::date('date', $exam->date,['class'=> 'form-control form-control-user', 'placeholder'=>
                        'วันที่']) !!}
                    </div>
                    <div class="form-group">
                        {{Form::label('start','เวลาเริ่มวิชา:')}}
                        {{ Form::time('start', $exam->start,['class'=> 'form-control form-control-user', 'placeholder'=> 'เวลาเริ่มวิชา']) }}
                    </div>
                    <div class="form-group">
                        {{Form::label('end','เวลาจบวิชา:')}}
                        {{ Form::time('end', $exam->end,['class'=> 'form-control form-control-user', 'placeholder'=> 'เวลาจบวิชา']) }}
                    </div>
                    <div class="form-group">
                        {{Form::label('teacher_id','ครูผู้คุม')}}
                        <select class="form-control" name="teacher_id">
                            @foreach ($teachers as $teacher)
                            <option value="{{ $teacher->id }}" @if ($teacher->id == old('teacher_id',
                                $exam->teacher_id))
                                selected="selected"
                                @endif
                                >{{ $teacher->firstName }} {{ $teacher->lastName }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        {{Form::label('type','ภาคเรียน')}}
                        {{-- {!! Form::select('type', array('Mid' => 'กลางภาค',' End'=>'ปลายภาค'),$exam->type,['class'=> 'form-control form-control-user']) !!} --}}
                        <select class="custom-select custom-select-sm" name="type">
                            @foreach ([
                            'Mid' => 'กลางภาค','End'=>'ปลายภาค'] as
                            $key => $type)
                            <option value="{{ $key }}" @if ($key==old($exam->type,
                                $exam->type))
                                selected="selected"
                                @endif
                                >{{ $type }}</option>
                            @endforeach
                        </select>
                    </div>
                    {{Form::hidden('_method','PUT')}}
                    {{Form::submit('บันทึก',['class'=>'btn btn-primary btn-user btn-block'])}}
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>

@endsection