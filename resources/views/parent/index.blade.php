@extends('layout.app')

@section('content')
{{-- <div>
    <h1>ข้อมูลผู้ปกครอง</h1>
    <a href="/parents/create" class="btn btn-primary">เพื่มข้อมูล</a>
    @if(count($parents) > 0)
    <ul class="list-group">
        @foreach($parents as $parent)
        <li class="list-group-item">
            <a href="/parents/{{$parent->id}}">{{$parent->firstName}} {{$parent->lastName}}</a>
        </li>
        @endforeach
    </ul>
    @else
    <h1>ไม่มีข้อมูลผู้ปกครอง<h1>
            @endif
</div> --}}

<div class="container-fluid">
        <!-- DataTales Example -->
    
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">ข้อมูลผู้ปกครอง</h6>
            </div>
            <a href="/parents/create" class="btn btn-primary">เพื่มข้อมูล</a>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>ชื่อ</th>
                                <th>ดูข้อมูล</th>
                                <th>แก้ไข</th>
                                <th>ลบ</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($parents as $parent)
    
                            <tr>
                                <td>{{$parent->firstName}} {{$parent->lastName}}</td>
                                
                                <td><a href="/parents/{{$parent->id}}" class="btn btn-info btn-icon-split">
                                        <span class="text">ข้อมูล</span>
                                    </a>
                                </td>
                                <td><a href="/parents/{{$parent->id}}/edit" class="btn btn-secondary btn-icon-split">
                                        <span class="text">แก้ไข</span>
                                    </a>
                                </td>
                                <td>
                                    {!! Form::open(['action' => ['ParentStudentController@destroy', $parent->id], 'method' =>
                                    'POST', 'class'=>'btn btn-danger btn-icon-split', 'onsubmit' => 'return confirm("ยืนยันต้องการลบข้อมูลหรือไม่ ?")']) !!}
                                    {{Form::hidden('_method','DELETE')}}
                                    {{Form::submit('ลบ',['class'=>'btn btn-danger'])}}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
    
                            @endforeach
    
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    
    </div>

@endsection