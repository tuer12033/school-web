@extends('layout.app')

@section('content')
{{-- <div>
    <h1>เพิ่มข้อมูลผู้ปกครอง<h1>
    {!! Form::open(['action' => 'ParentStudentController@store', 'method' => 'POST']) !!}
    <div class="form-group">
        {{Form::label('firstName','ชื่อ')}}
        {{Form::text('firstName','',['class'=> 'form-control', 'placeholder'=> 'ชื่อ'])}}
    </div>
    <div class="form-group">
        {{Form::label('lastName','นามสกุล')}}
        {{Form::text('lastName','',['class'=> 'form-control', 'placeholder'=> 'นามสกุล'])}}
    </div>
    <div class="form-group">
        {{Form::label('address','ที่อยู่')}}
        {{Form::text('address','',['class'=> 'form-control', 'placeholder'=> 'ที่อยู่'])}}
    </div>
    <div class="form-group">
        {{Form::label('tel','เบอร์ติดต่อ')}}
        {{Form::text('tel','',['class'=> 'form-control', 'placeholder'=> 'เบอร์ติดต่อ'])}}
    </div>
    {{Form::submit('บันทึก',['class'=>'btn btn-primary'])}}
    {!! Form::close() !!}

</div> --}}
<div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
            <div class="row">
                <div class="col-lg">
                    <div class="p-5">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">เพิ่มข้อมูลผู้ปกครอง</h1>
                        </div>
                        {!! Form::open(['action' => 'ParentStudentController@store', 'method' => 'POST']) !!}
                        <div class="form-group row">
    
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                {{Form::label('date','ชื่อ')}}<span style="color:red">*</span>
                                {{Form::text('firstName','',['class'=> 'form-control form-control-user', 'placeholder'=> 'ชื่อ', 'maxlength'=>"50"])}}
                            </div>
                            <div class="col-sm-6">
                                {{Form::label('date','นามสกุล')}}<span style="color:red">*</span>
                                {{Form::text('lastName','',['class'=> 'form-control form-control-user', 'placeholder'=> 'นามสกุล', 'maxlength'=>"50"])}}
                            </div>
    
                        </div>
                        <div class="form-group">
                            {{Form::label('date','ที่อยู่')}}<span style="color:red">*</span>
                            {{Form::text('address','',['class'=> 'form-control form-control-user', 'placeholder'=> 'ที่อยู่', 'maxlength'=>"50"])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('date','เบอร์ติดต่อ')}}<span style="color:red">*</span>
                            {{Form::number('tel','', ['class'=> 'form-control form-control-user', 'placeholder'=> 'เบอร์ติดต่อ', 'maxlength'=>"10"])}}
                        </div>
                        {{Form::submit('บันทึก',['class'=>'btn btn-primary btn-user btn-block'])}}
                        {!! Form::close() !!}
    
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection