@extends('layout.app')

@section('content')
<div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
        <div class="row">
            <div class="col-lg">
                <div class="p-5">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">ข้อมูลผู้ปกครอง</h1>
                    </div>

                    <div class="form-group row">

                        <div class="col-sm-6 mb-3 mb-sm-0">
                            {{Form::text('firstName',$parent->firstName,['class'=> 'form-control form-control-user', 'readonly', 'placeholder'=> 'ชื่อ', 'maxlength'=>"50"])}}
                        </div>
                        <div class="col-sm-6">
                            {{Form::text('lastName',$parent->lastName,['class'=> 'form-control form-control-user', 'readonly', 'placeholder'=> 'นามสกุล', 'maxlength'=>"50"])}}
                        </div>

                    </div>
                    <div class="form-group">
                        {{Form::text('address',$parent->address,['class'=> 'form-control form-control-user', 'readonly', 'placeholder'=> 'ที่อยู่', 'maxlength'=>"50"])}}
                    </div>
                    <div class="form-group">
                        {{Form::text('tel',$parent->tel,['class'=> 'form-control form-control-user', 'readonly', 'placeholder'=> 'เบอร์ติดต่อ', 'maxlength'=>"20"])}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">รายชื่อนักเรียน</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>รหัส</th>
                        <th>ชื่อ</th>
                        <th>นามสกุล</th>
                        <th>ห้อง</th>
                        <th>ผู้ปกครอง</th>
                        <th>ดูข้อมูล</th>
                        <th>แก้ไข</th>
                        <th>ลบ</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($students as $student)

                    <tr>
                        <td>{{$student->code}}</td>
                        <td>{{$student->firstName}}</td>
                        <td>{{$student->lastName}}</td>
                        <td>{{$student->room->roomName}}</td>
                        <td>{{$student->parent->firstName}}
                            {{$student->parent->lastName}}
                        </td>
                        <td><a href="/students/{{$student->id}}" class="btn btn-info btn-icon-split">
                                <span class="text">ข้อมูล</span>
                            </a>
                        </td>
                        <td><a href="/students/{{$student->id}}/edit" class="btn btn-secondary btn-icon-split">
                                <span class="text">แก้ไข</span>
                            </a>
                        </td>
                        <td>
                            {!! Form::open(['action' => ['StudentController@destroy', $student->id], 'method' =>
                            'POST', 'class'=>'btn btn-danger btn-icon-split']) !!}
                            {{Form::hidden('_method','DELETE')}}
                            {{Form::submit('ลบ',['class'=>'btn btn-danger'])}}
                            {!! Form::close() !!}
                        </td>
                    </tr>

                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection