@extends('layout.app')

@section('content')
<div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
        <div class="row">
            <div class="col-lg">
                <div class="p-5">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">เพิ่มข้อมูลระดับชั้น</h1>
                    </div>
                    {!! Form::open(['action' => 'ClassLevelController@store', 'method' => 'POST'])
                    !!}
                    <div class="form-group">
                        {{Form::label('teacher_id','ชื่อระดับชั้น')}}<span style="color:red">*</span>
                        {{Form::text('name','',['class'=> 'form-control form-control-user', 'placeholder'=> 'ชื่อระดับชั้น', 'maxlength'=>"50"])}}
                    </div>
                    {{Form::submit('บันทึก',['class'=>'btn btn-primary btn-user btn-block'])}}
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>

@endsection