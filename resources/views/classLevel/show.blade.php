@extends('layout.app')

@section('content')
<div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
        <div class="row">
            <div class="col-lg">
                <div class="p-5">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">ข้อมูลระดับชั้น</h1>
                    </div>
                    <div class="form-group">
                        {{Form::text('code',$classLevel->name,['class'=> 'form-control form-control-user', 'readonly', 'placeholder'=> 'ห้องเรียน', 'maxlength'=>"50"])}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection