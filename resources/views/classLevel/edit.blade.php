@extends('layout.app')

@section('content')
<div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
        <div class="row">
            <div class="col-lg">
                <div class="p-5">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">แก้ไขข้อมูลระดับชั้น</h1>
                    </div>
                    {!! Form::open(['action' => ['ClassLevelController@update', $classLevel->id], 'method' => 'POST'])
                    !!}
                    <div class="form-group">
                        {{Form::text('name',$classLevel->name,['class'=> 'form-control form-control-user', 'placeholder'=> 'ห้องเรียน', 'maxlength'=>"50"])}}
                    </div>
                    {{Form::hidden('_method','PUT')}}
                    {{Form::submit('บันทึก',['class'=>'btn btn-primary btn-user btn-block'])}}
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>

@endsection