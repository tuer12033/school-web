@extends('layout.app')

@section('content')

<div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
        <div class="row">
            <div class="col-lg">
                <div class="p-5">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">เพิ่มข้อมูลเวลาเรียน</h1>
                    </div>
                    {!! Form::open(['action' => 'ClassTimeController@store', 'method' => 'POST']) !!}
                    <div class="form-group">
                        {{Form::text('number','',['class'=> 'form-control form-control-user', 'placeholder'=> 'ลำดับวิชา', 'maxlength'=>"3"])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('date','วัน')}}
                        {!! Form::select('date', [null => 'เลือกวัน'] + ['Monday' => 'จันทร์','
                        Tuesday'=>'อังคาร','Wednesday'=>'พุธ','Thursday'=>'พฤหัสบดี','Friday'=>'ศุกร์'], null, ['class'
                        => 'form-control form-control-user']) !!}
                    </div>
                    <div class="form-group">
                        {{Form::label('start','เวลาเริ่มวิชา:')}}
                        {{ Form::time('start', '00:00',['class'=> 'form-control form-control-user', 'placeholder'=> 'นามสกุล', 'maxlength'=>"50"]) }}
                    </div>
                    <div class="form-group">
                        {{Form::label('end','เวลาจบวิชา:')}}
                        {{ Form::time('end', '00:00',['class'=> 'form-control form-control-user', 'placeholder'=> 'นามสกุล', 'maxlength'=>"50"]) }}
                    </div>
                    {{Form::submit('บันทึก',['class'=>'btn btn-primary btn-user btn-block'])}}
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>

@endsection