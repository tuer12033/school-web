@extends('layout.app')

@section('content')
<div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
        <div class="row">
            <div class="col-lg">
                <div class="p-5">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">แก้ไขข้อมูลเวลาเรียน</h1>
                    </div>
                    {!! Form::open(['action' => ['ClassTimeController@update', $classTime->id], 'method' => 'POST']) !!}
                    <div class="form-group">
                        {{Form::text('number',$classTime->number,['class'=> 'form-control form-control-user', 'placeholder'=> 'ลำดับวิชา', 'maxlength'=>"3"])}}
                    </div>
                    <div class="form-group">
                        <h1 class="h6 text-gray-900 mb-4">เลือกวัน</h1>
                        <select class="custom-select custom-select-sm" name="date">
                            @foreach (['Monday' =>
                            'จันทร์','Tuesday'=>'อังคาร','Wednesday'=>'พุธ','Thursday'=>'พฤหัสบดี','Friday'=>'ศุกร์'] as
                            $key => $date)
                            <option value="{{ $key }}" @if ($key==old($classTime->date,
                                $classTime->date))
                                selected="selected"
                                @endif
                                >{{ $date }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        {{Form::label('start','เวลาเริ่มวิชา:')}}
                        {{ Form::time('start', $classTime->start,['class'=> 'form-control form-control-user', 'placeholder'=> 'นามสกุล', 'maxlength'=>"50"]) }}
                    </div>
                    <div class="form-group">
                        {{Form::label('end','เวลาจบวิชา:')}}
                        {{ Form::time('end', $classTime->end,['class'=> 'form-control form-control-user', 'placeholder'=> 'นามสกุล', 'maxlength'=>"50"]) }}
                    </div>
                    {{Form::hidden('_method','PUT')}}
                    {{Form::submit('บันทึก',['class'=>'btn btn-primary btn-user btn-block'])}}
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>

@endsection