@extends('layout.app')

@section('content')
<div class="container-fluid">
    <!-- DataTales Example -->

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">ข้อมูลตารางสอน</h6>
        </div>
        <a href="/timetables/create/{{$room->id}}" class="btn btn-primary">เพื่มตารางเรียน</a>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>วัน</th>
                            <th>เวลา</th>
                            <th>วิชา</th>
                            <th>ดูข้อมูล</th>
                            <th>แก้ไข</th>
                            <th>ลบ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($timetables as $timetable)
                        <tr>
                            <td>{{$timetable->classTime->date}}</td>
                            <td>{{$timetable->classTime->start}} - {{$timetable->classTime->end}}</td>
                            <td>{{$timetable->subject->name}}</td>
                            <td><a href="/timetables/{{$timetable->id}}" class="btn btn-info btn-icon-split">
                                    <span class="text">ดูข้อมูล</span>
                                </a>
                            </td>
                            <td><a href="/timetables/{{$timetable->id}}/edit" class="btn btn-secondary btn-icon-split">
                                    <span class="text">แก้ไข</span>
                                </a>
                            </td>
                            <td>
                                {!! Form::open(['action' => ['TimetableController@destroy', $timetable->id], 'method' =>
                                'POST', 'class'=>'btn btn-danger btn-icon-split', 'onsubmit' => 'return confirm("ยืนยันต้องการลบข้อมูลหรือไม่ ?")']) !!}
                                {{Form::hidden('_method','DELETE')}}
                                {{Form::submit('ลบ',['class'=>'btn btn-danger'])}}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

@endsection