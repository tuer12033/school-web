@extends('layout.app')

@section('content')
{{-- <div>
    <h1>เพิ่มข้อมูลนักเรียน<h1>
    {!! Form::open(['action' => ['StudentController@update', $student->id], 'method' => 'POST']) !!}
    <div class="form-group">
        {{Form::label('code','รหัส')}}
        {{Form::text('code',$student->code,['class'=> 'form-control', 'placeholder'=> 'รหัส'])}}
    </div>
    <div class="form-group">
        {{Form::label('firstName','ชื่อ')}}
        {{Form::text('firstName',$student->firstName,['class'=> 'form-control', 'placeholder'=> 'ชื่อ'])}}
    </div>
    <div class="form-group">
        {{Form::label('lastName','นามสกุล')}}
        {{Form::text('lastName',$student->lastName,['class'=> 'form-control', 'placeholder'=> 'นามสกุล'])}}
    </div>
    {{Form::hidden('_method','PUT')}}
    {{Form::submit('บันทึก',['class'=>'btn btn-primary'])}}
    {!! Form::close() !!}

</div> --}}
<div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
            <div class="row">
                <div class="col-lg">
                    <div class="p-5">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">แก้ไขข้อมูลตารางเรียน</h1>
                        </div>
                        {!! Form::open(['action' => ['TimetableController@update', $timetable->id], 'method' => 'POST']) !!}
                        
                        <div class="form-group">
                            <h1 class="h6 text-gray-900 mb-4">เลือกห้อง</h1>
                            <select class="custom-select custom-select-sm" name="room_id">
                                @foreach ($rooms as $room)
                                <option value="{{ $room->id }}" @if ($room->id == old('room_id', $timetable->room_id))
                                    selected="selected"
                                    @endif
                                    >{{ $room->roomName }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                                <h1 class="h6 text-gray-900 mb-4">เลือกวิชา</h1>
                                <select class="custom-select custom-select-sm" name="subject_id">
                                    @foreach ($subjects as $subject)
                                    <option value="{{ $subject->id }}" @if ($subject->id == old('subject_id', $timetable->subject_id))
                                        selected="selected"
                                        @endif
                                        >{{ $subject->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                    <h1 class="h6 text-gray-900 mb-4">เลือกเวลาเรียน</h1>
                                    <select class="custom-select custom-select-sm" name="class_time_id">
                                        @foreach ($classTimes as $classTime)
                                        <option value="{{ $classTime->id }}" @if ($classTime->id == old('classTime_id', $timetable->class_time_id))
                                            selected="selected"
                                            @endif
                                            >{{ $classTime->date }} {{ $classTime->start }} - {{ $classTime->end }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                        <h1 class="h6 text-gray-900 mb-4">เลือกผู้คุมสอน</h1>
                                        <select class="custom-select custom-select-sm" name="teacher_id">
                                            @foreach ($teachers as $teacher)
                                            <option value="{{ $teacher->id }}" @if ($teacher->id == old('teacher_id', $timetable->teacher_id))
                                                selected="selected"
                                                @endif
                                                >{{ $teacher->firstName }} {{ $teacher->lastName }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                        {{Form::hidden('_method','PUT')}}
                        {{Form::submit('บันทึก',['class'=>'btn btn-primary btn-user btn-block'])}}
                        {!! Form::close() !!}
    
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection