@extends('layout.app')

@section('content')

<div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
        <div class="row">
            <div class="col-lg">
                <div class="p-5">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">เพิ่มตารางเรียน</h1>
                    </div>
                    {!! Form::open(['action' => 'TimetableController@store', 'method' => 'POST']) !!}

                    <div class="form-group">
                        <h1 class="h6 text-gray-900 mb-4">ห้อง<span style="color:red">*</span></h1>
                        <select class="custom-select custom-select-sm" name="room_id">
                            <option value={{$room->id}}>{{$room->roomName}}</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <h1 class="h6 text-gray-900 mb-4">เลือกวิชา<span style="color:red">*</span></h1>
                        <select class="custom-select custom-select-sm" name="subject_id">
                            @foreach($subjects as $subject)
                            <option value={{$subject->id}}>{{$subject->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <h1 class="h6 text-gray-900 mb-4">เลือกเวลาเรียน<span style="color:red">*</span></h1>
                        <select class="custom-select custom-select-sm" name="class_time_id">
                            @foreach($classTimes as $classTime)
                            <option value={{$classTime->id}}>{{$classTime->date}} {{$classTime->start}} -
                                {{$classTime->end}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <h1 class="h6 text-gray-900 mb-4">เลือกผู้คุมสอน<span style="color:red">*</span></h1>
                        <select class="custom-select custom-select-sm" name="teacher_id">
                            @foreach($teachers as $teacher)
                            <option value={{$teacher->id}}>{{$teacher->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    {{Form::submit('บันทึก',['class'=>'btn btn-primary btn-user btn-block'])}}
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>

@endsection