@extends('layout.app')

@section('content')
<div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
        <div class="row">
            <div class="col-lg">
                <div class="p-5">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">ข้อมูลวิชา</h1>
                    </div>
                    <div class="form-group">
                        {{Form::text('subjectCode',$timetable->classTime->date,['class'=> 'form-control form-control-user', 'readonly', 'placeholder'=> 'รหัสวิชา', 'maxlength'=>"50"])}}
                    </div>
                    <div class="form-group">
                        {{Form::text('name',$timetable->classTime->start.' - '.$timetable->classTime->end,['class'=> 'form-control form-control-user', 'readonly', 'placeholder'=> 'ชื่อวิชา', 'maxlength'=>"50"])}}
                    </div>
                    <div class="form-group">
                        {{Form::text('address',$timetable->subject->name,['class'=> 'form-control form-control-user', 'readonly', 'placeholder'=> 'ระดับชั้น'])}}
                    </div>
                    <div class="form-group">
                        {{Form::text('address',$timetable->teacher->firstName.' '.$timetable->teacher->lastName,['class'=> 'form-control form-control-user', 'readonly', 'placeholder'=> 'ระดับชั้น'])}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection