@extends('layout.app')

@section('content')
{{-- <div>
    <h1>เพิ่มข้อมูลนักเรียน</h1>
    {!! Form::open(['action' => ['SubjectController@update', $subject->id], 'method' => 'POST']) !!}
    <div class="form-group">
        {{Form::label('name','ชื่อ')}}
{{Form::text('name',$subject->name,['class'=> 'form-control', 'placeholder'=> 'ชื่อ'])}}
</div>
<div class="form-group">
    {{Form::label('subjectCode','รหัสวิชา')}}
    {{Form::text('subjectCode',$subject->subjectCode,['class'=> 'form-control', 'placeholder'=> 'รหัสวิชา'])}}
</div>
<div class="form-group">
    {{Form::label('classLevel_id','ระดับชั้น')}}
    <select class="form-control" name="classLevel_id">
        @foreach ($classLevels as $classLevel)
        <option value="{{ $classLevel->id }}" {{ ( $classLevel->id == $subject->classLevel_id) }}>
            {{ $classLevel->name }} </option>
        @endforeach
    </select>
</div>
{{Form::hidden('_method','PUT')}}
{{Form::submit('บันทึก',['class'=>'btn btn-primary'])}}
{!! Form::close() !!}

</div> --}}
<div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
        <div class="row">
            <div class="col-lg">
                <div class="p-5">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">แก้ไขข้อมูลวิชา</h1>
                    </div>
                    {!! Form::open(['action' => ['SubjectController@update', $subject->id], 'method' => 'POST']) !!}
                    <div class="form-group">
                        {{Form::text('subjectCode',$subject->subjectCode,['class'=> 'form-control form-control-user', 'placeholder'=> 'รหัสวิชา', 'maxlength'=>"20"])}}
                    </div>
                    <div class="form-group">
                        {{Form::text('name',$subject->name,['class'=> 'form-control form-control-user', 'placeholder'=> 'ชื่อวิชา', 'maxlength'=>"50"])}}
                    </div>
                    <div class="form-group">
                        <h1 class="h6 text-gray-900 mb-4">เลือกระดับชั้น</h1>
                        <select class="custom-select custom-select-sm" name="classLevel_id">
                            @foreach ($classLevels as $classLevel)
                            <option value="{{ $classLevel->id }}" @if ($classLevel->id == old('classLevel_id', $subject->classLevel_id))
                                selected="selected"
                                @endif
                                >{{ $classLevel->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    {{Form::hidden('_method','PUT')}}
                    {{Form::submit('บันทึก',['class'=>'btn btn-primary btn-user btn-block'])}}
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>

@endsection