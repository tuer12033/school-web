@extends('layout.app')

@section('content')
<div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
        <div class="row">
            <div class="col-lg">
                <div class="p-5">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">เพิ่มข้อมูลวิชา</h1>
                    </div>
                    {!! Form::open(['action' => 'SubjectController@store', 'method' => 'POST']) !!}

                    <div class="form-group">
                        {{Form::label('date','รหัสวิชา')}}<span style="color:red">*</span>
                        {{Form::text('subjectCode','',['class'=> 'form-control form-control-user', 'placeholder'=> 'รหัสวิชา', 'maxlength'=>"20"])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('date','ชื่อ')}}<span style="color:red">*</span>
                        {{Form::text('name','',['class'=> 'form-control form-control-user', 'placeholder'=> 'ชื่อ', 'maxlength'=>"50"])}}
                    </div>
                    <div class="form-group">
                        <h1 class="h6 text-gray-900 mb-4">เลือกระดับชั้น<span style="color:red">*</span></h1>
                        <select class="custom-select custom-select-sm" name="classLevel_id">
                            @foreach($classLevels as $classLevel)
                            <option value={{$classLevel->id}}>{{$classLevel->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    {{Form::submit('บันทึก',['class'=>'btn btn-primary btn-user btn-block'])}}
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>

@endsection