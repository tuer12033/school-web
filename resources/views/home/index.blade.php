@extends('layout.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        {{-- <div class="col-md-8">
            <div class="card row justify-content-center align-content-center">
                <h1 >เข้าสู่ระบบ</h1>
                <div class="card-header">สำหรับแอดมิน</div>
                <div class="card-header">สำหรับอาจารน์</div>
                <div class="card-header">สำหรับนักเรียน</div>
            </div>
        </div> --}}
        <ul class="list-group col-md-8">
            <li class="list-group-item active">เข้าสู่ระบบ</li>
            <a href="{{ route('login', 'ADMIN') }}">
                <li class="list-group-item">สำหรับแอดมิน</li>
            </a>
            <a href="{{ route('login', 'TEACHER') }}">
                <li class="list-group-item">สำหรับอาจารน์</li>
            </a>
            <a href="{{ route('login', 'BOSS') }}">
                <li class="list-group-item">สำหรับผู้อำนวยการ</li>
            </a>
        </ul>
    </div>
</div>
@endsection