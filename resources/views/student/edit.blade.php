@extends('layout.app')

@section('content')

<div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
        <div class="row">
            <div class="col-lg">
                <div class="p-5">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">แก้ไขข้อมูลนักเรียน</h1>
                    </div>
                    {!! Form::open(['action' => ['StudentController@update', $student->id], 'method' => 'POST']) !!}
                    <div class="form-group">
                        {{Form::text('code',$student->code,['class'=> 'form-control form-control-user', 'placeholder'=> 'รหัส', 'maxlength'=>"20"])}}
                    </div>
                    <div class="form-group row">

                        <div class="col-sm-6 mb-3 mb-sm-0">
                            {{Form::text('firstName',$student->firstName,['class'=> 'form-control form-control-user', 'placeholder'=> 'ชื่อ', 'maxlength'=>"50"])}}
                        </div>
                        <div class="col-sm-6">
                            {{Form::text('lastName',$student->lastName,['class'=> 'form-control form-control-user', 'placeholder'=> 'นามสกุล', 'maxlength'=>"50"])}}
                        </div>
                    </div>
                    <div class="form-group">
                        <h1 class="h6 text-gray-900 mb-4">เลือกระดับชั้น</h1>
                        <select class="custom-select custom-select-sm" name="classLevel_id">
                            @foreach ($classLevels as $classLevel)
                            <option value="{{ $classLevel->id }}" @if ($classLevel->id == old('classLevel_id', $student->classLevel_id))
                                selected="selected"
                                @endif
                                >{{ $classLevel->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <h1 class="h6 text-gray-900 mb-4">เลือกห้องเรียน</h1>
                        <select class="custom-select custom-select-sm" name="room_id">
                            @foreach ($rooms as $room)
                            <option value="{{ $room->id }}" @if ($room->id == old('room_id', $student->room_id))
                                selected="selected"
                                @endif
                                >{{ $room->roomName }}</option>
                            @endforeach
                        </select>

                    </div>
                    <div class="form-group">
                        <h1 class="h6 text-gray-900 mb-4">เลือกผู้ปกครอง</h1>
                        <select class="custom-select custom-select-sm" name="parent_student_id">
                            @foreach ($parents as $parent)
                            <option value="{{ $parent->id }}" @if ($parent->id == old('room_id',
                                $student->parent_student_id))
                                selected="selected"
                                @endif
                                >{{ $parent->firstName }} {{ $parent->lastName }}</option>
                            @endforeach
                        </select>
                    </div>
                    {{Form::hidden('_method','PUT')}}
                    {{Form::submit('บันทึก',['class'=>'btn btn-primary btn-user btn-block'])}}
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>

@endsection