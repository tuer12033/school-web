@extends('layout.app')

@section('content')

<div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
        <div class="row">
            <div class="col-lg">
                <div class="p-5">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">ข้อมูลนักเรียน</h1>
                    </div>
                    
                    <div class="form-group">
                        {{Form::text('code',$student->code,['class'=> 'form-control form-control-user', 'readonly', 'placeholder'=> 'รหัส', 'maxlength'=>"50"])}}
                    </div>
                    <div class="form-group row">

                        <div class="col-sm-6 mb-3 mb-sm-0">
                            {{Form::text('firstName',$student->firstName,['class'=> 'form-control form-control-user', 'readonly', 'placeholder'=> 'ชื่อ', 'maxlength'=>"50"])}}
                        </div>
                        <div class="col-sm-6">
                            {{Form::text('lastName',$student->lastName,['class'=> 'form-control form-control-user', 'readonly', 'placeholder'=> 'นามสกุล', 'maxlength'=>"50"])}}
                        </div>
                    </div>
                    <div class="form-group">
                        <h1 class="h6 text-gray-900 mb-4">ห้องเรียน</h1>
                        <select disabled class="custom-select custom-select-sm" name="room_id">
                            @foreach ($rooms as $room)
                            <option value="{{ $room->id }}" @if ($room->id == old('room_id', $student->room_id))
                                selected="selected"
                                @endif
                                >{{ $room->roomName }}</option>
                            @endforeach
                        </select>

                    </div>
                    <div class="form-group">
                        <h1 class="h6 text-gray-900 mb-4">ผู้ปกครอง</h1>
                        <select disabled class="custom-select custom-select-sm" name="parent_student_id">
                            @foreach ($parents as $parent)
                            <option value="{{ $parent->id }}" @if ($parent->id == old('room_id',
                                $student->parent_student_id))
                                selected="selected"
                                @endif
                                >{{ $parent->firstName }} {{ $parent->lastName }}</option>
                            @endforeach
                        </select>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection