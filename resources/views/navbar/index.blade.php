{{-- <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
  <div class="container">
    <a class="navbar-brand" href="{{ url('/') }}">
SCHOOL
</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
  aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
  <span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse" id="navbarSupportedContent">
  <!-- Left Side Of Navbar -->
  <ul class="navbar-nav mr-auto">
    <li class="nav-item">
      <a class="list-group-item list-group-item-action bg-light" href="/students">ข้อมูลนักเรียน</a>
    </li>
    <li class="nav-item">
      <a class="list-group-item list-group-item-action bg-light" href="/rooms">ข้อมูลห้องเรียน</a>
    </li>
    <li class="nav-item">
      <a class="list-group-item list-group-item-action bg-light" href="/parents">ข้อมูลผู้ปกครอง</a>
    </li>
    <li class="nav-item">
      <a class="list-group-item list-group-item-action bg-light" href="/classTimes">ข้อมูลเวลาเรียน</a>
    </li>
    <li class="nav-item">
      <a class="list-group-item list-group-item-action bg-light" href="/subjects">ข้อมูลรายวิชา</a>
    </li>
    <li class="nav-item">
      <a class="list-group-item list-group-item-action bg-light" href="/timetables">ข้อมูลตารางเรียน</a>
    </li>
    <li class="nav-item">
      <a class="list-group-item list-group-item-action bg-light" href="/classLevels">ข้อมูลระดับชั้น</a>
    </li>
    <li class="nav-item">
      <a class="list-group-item list-group-item-action bg-light" href="/exams">ข้อมูลตารางสอบ</a>
    </li>
    <li class="nav-item">
      <a class="list-group-item list-group-item-action bg-light" href="/checkName">ข้อมูลเช็คชื่อ</a>
    </li>
    <li class="nav-item">
      <a class="list-group-item list-group-item-action bg-light" href="/informations">ข้อมูลประชาสัมพันธ์</a>
    </li>

  </ul>

  <!-- Right Side Of Navbar -->
  <ul class="navbar-nav ml-auto">
    <!-- Authentication Links -->
    @guest
    <li class="nav-item">
      <a class="list-group-item list-group-item-action bg-light" href="{{ route('login') }}">{{ __('Login') }}</a>
    </li>
    @if (Route::has('register'))
    <li class="nav-item">
      <a class="list-group-item list-group-item-action bg-light" href="{{ route('register') }}">{{ __('Register') }}</a>
    </li>
    @endif
    @else
    <li class="nav-item dropdown">
      <a id="navbarDropdown" class="list-group-item list-group-item-action bg-light dropdown-toggle" href="#" role="button" data-toggle="dropdown"
        aria-haspopup="true" aria-expanded="false" v-pre>
        {{ Auth::user()->name }} <span class="caret"></span>
      </a>

      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();">
          {{ __('Logout') }}
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
        </form>
      </div>
    </li>
    @endguest
  </ul>
</div>
</div>
</nav> --}}
<div class="bg-light border-right" id="sidebar-wrapper">
  <div class="sidebar-heading">SCHOOL</div>
  <div class="list-group list-group-flush">
      {{-- @guest --}}
    @if (Auth::check() && Auth::user()->isTeacher())
    <a class="list-group-item list-group-item-action bg-light" href="/students">ข้อมูลนักเรียน</a>
    <a class="list-group-item list-group-item-action bg-light" href="/rooms">ข้อมูลห้องเรียน</a>
    <a class="list-group-item list-group-item-action bg-light" href="/parents">ข้อมูลผู้ปกครอง</a>
    <a class="list-group-item list-group-item-action bg-light" href="/classTimes">ข้อมูลเวลาเรียน</a>
    <a class="list-group-item list-group-item-action bg-light" href="/subjects">ข้อมูลรายวิชา</a>
    <a class="list-group-item list-group-item-action bg-light" href="/timetables">ข้อมูลตารางเรียน</a>
    <a class="list-group-item list-group-item-action bg-light" href="/classLevels">ข้อมูลระดับชั้น</a>
    <a class="list-group-item list-group-item-action bg-light" href="/exams">ข้อมูลตารางสอบ</a>
    <a class="list-group-item list-group-item-action bg-light" href="/schoolRecords">ข้อมูลคะแนนเก็บ</a>
    <a class="list-group-item list-group-item-action bg-light" href="/checkNames">ข้อมูลเช็คชื่อ</a>
    <a class="list-group-item list-group-item-action bg-light" href="/checkLineUps">ข้อมูลเช็คชื่อเข้าแถว</a>
    <a class="list-group-item list-group-item-action bg-light" href="/informations">ข้อมูลประชาสัมพันธ์</a>
    <a class="list-group-item list-group-item-action bg-light" href="/visitHomes">ข้อมูลเยี่ยมบ้าน</a>
    <a class="list-group-item list-group-item-action bg-light" href="/messages">จดหมาย</a>
    @endif
    @if (Auth::check() && Auth::user()->isAdmin())
    <a class="list-group-item list-group-item-action bg-light" href="/students">ข้อมูลนักเรียน</a>
    <a class="list-group-item list-group-item-action bg-light" href="/rooms">ข้อมูลห้องเรียน</a>
    <a class="list-group-item list-group-item-action bg-light" href="/parents">ข้อมูลผู้ปกครอง</a>
    <a class="list-group-item list-group-item-action bg-light" href="/classLevels">ข้อมูลระดับชั้น</a>
    <a class="list-group-item list-group-item-action bg-light" href="/teachers">ข้อมูลผู้ใช้</a>
    <a class="list-group-item list-group-item-action bg-light" href="/informations">ข้อมูลประชาสัมพันธ์</a>
    @endif
    @if (Auth::check() && Auth::user()->isBoss())
    <a class="list-group-item list-group-item-action bg-light" href="/students">ข้อมูลนักเรียน</a>
    <a class="list-group-item list-group-item-action bg-light" href="/rooms">ข้อมูลห้องเรียน</a>
    <a class="list-group-item list-group-item-action bg-light" href="/parents">ข้อมูลผู้ปกครอง</a>
    <a class="list-group-item list-group-item-action bg-light" href="/classTimes">ข้อมูลเวลาเรียน</a>
    <a class="list-group-item list-group-item-action bg-light" href="/subjects">ข้อมูลรายวิชา</a>
    <a class="list-group-item list-group-item-action bg-light" href="/timetables">ข้อมูลตารางเรียน</a>
    <a class="list-group-item list-group-item-action bg-light" href="/classLevels">ข้อมูลระดับชั้น</a>
    <a class="list-group-item list-group-item-action bg-light" href="/exams">ข้อมูลตารางสอบ</a>
    <a class="list-group-item list-group-item-action bg-light" href="/schoolRecords">ข้อมูลคะแนนเก็บ</a>
    <a class="list-group-item list-group-item-action bg-light" href="/checkNames">ข้อมูลเช็คชื่อ</a>
    <a class="list-group-item list-group-item-action bg-light" href="/checkLineUps">ข้อมูลเช็คชื่อเข้าแถว</a>
    <a class="list-group-item list-group-item-action bg-light" href="/informations">ข้อมูลประชาสัมพันธ์</a>
    <a class="list-group-item list-group-item-action bg-light" href="/visitHomes">ข้อมูลเยี่ยมบ้าน</a>
    <a class="list-group-item list-group-item-action bg-light" href="/messages">จดหมาย</a>
    <a class="list-group-item list-group-item-action bg-light" href="/teachers">ข้อมูลผู้ใช้</a>
    @endif
  </div>
  {{-- @endguest --}}
</div>