@extends('layout.app')

@section('content')
<div class="container-fluid">
    <!-- DataTales Example -->

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">ข้อมูลคะแนนเก็บ</h6>
        </div>
        <a href="/schoolRecords/create" class="btn btn-primary">เพื่มข้อมูล</a>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>นักเรียน</th>
                            <th>วิชา</th>
                            <th>ภาคเรียน</th>
                            <th>คะแนนเต็ม</th>
                            <th>คะแนน</th>
                            <th>ดูข้อมูล</th>
                            <th>แก้ไข</th>
                            <th>ลบ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($schoolRecords as $schoolRecord)

                        <tr>
                            <td>{{$schoolRecord->student->firstName}} {{$schoolRecord->student->lsatName}}</td>
                            <td>{{$schoolRecord->subject->name}}</td>
                            <td>{{$schoolRecord->year}}/{{$schoolRecord->term}}</td>
                            <td>{{$schoolRecord->score_max}}</td>
                            <td>{{$schoolRecord->score}}</td>

                            <td><a href="/schoolRecords/{{$schoolRecord->id}}" class="btn btn-info btn-icon-split">
                                    <span class="text">ข้อมูล</span>
                                </a>
                            </td>
                            <td><a href="/schoolRecords/{{$schoolRecord->id}}/edit" class="btn btn-secondary btn-icon-split">
                                    <span class="text">แก้ไข</span>
                                </a>
                            </td>
                            <td>
                                {!! Form::open(['action' => ['SchoolRecordController@destroy', $schoolRecord->id], 'method' =>
                                'POST', 'class'=>'btn btn-danger btn-icon-split', 'onsubmit' => 'return confirm("ยืนยันต้องการลบข้อมูลหรือไม่ ?")']) !!}
                                {{Form::hidden('_method','DELETE')}}
                                {{Form::submit('ลบ',['class'=>'btn btn-danger'])}}
                                {!! Form::close() !!}
                            </td>
                        </tr>

                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
@endsection