@extends('layout.app')

@section('content')

<div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
        <div class="row">
            <div class="col-lg">
                <div class="p-5">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">แก้ไขคะแนนเก็บ</h1>
                    </div>
                    {!! Form::open(['action' => ['SchoolRecordController@update', $schoolRecord->id], 'method' => 'POST']) !!}
                    <div class="form-group">
                        {{Form::label('student_id','นักเรียน')}}
                        <select class="form-control" name="student_id">
                            @foreach ($students as $student)
                            <option value="{{ $student->id }}" @if ($student->id == old('student_id',
                                $schoolRecord->student_id))
                                selected="selected"
                                @endif
                                >{{ $student->firstName }} {{ $student->lastName }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        {{Form::label('subject_id','วิชา')}}
                        <select class="form-control" name="subject_id">
                            @foreach ($subjects as $subject)
                            <option value="{{ $subject->id }}" @if ($subject->id == old('subject_id',
                                $schoolRecord->subject_id))
                                selected="selected"
                                @endif
                                >{{ $subject->name }} </option>
                            @endforeach
                        </select>
                    </div>
                    {{-- <div class="form-group">
                        {{Form::label('date','วัน')}}
                        {!! Form::date('date', $exam->date,['class'=> 'form-control form-control-user', 'placeholder'=>
                        'วันที่']) !!}
                    </div>
                    <div class="form-group">
                        {{Form::label('start','เวลาเริ่มวิชา:')}}
                        {{ Form::time('start', $exam->start,['class'=> 'form-control form-control-user', 'placeholder'=> 'เวลาเริ่มวิชา']) }}
                    </div>
                    <div class="form-group">
                        {{Form::label('end','เวลาจบวิชา:')}}
                        {{ Form::time('end', $exam->end,['class'=> 'form-control form-control-user', 'placeholder'=> 'เวลาจบวิชา']) }}
                    </div>
                    <div class="form-group">
                        {{Form::label('teacher_id','ครูผู้คุม')}}
                        <select class="form-control" name="teacher_id">
                            @foreach ($teachers as $teacher)
                            <option value="{{ $teacher->id }}" @if ($teacher->id == old('teacher_id',
                                $exam->teacher_id))
                                selected="selected"
                                @endif
                                >{{ $teacher->firstName }} {{ $teacher->lastName }}</option>
                            @endforeach
                        </select>
                    </div> --}}
                    <div class="form-group">
                        <h1 class="h6 text-gray-900 mb-4">ปีการศึกษา</h1>
                        {{Form::text('year',$schoolRecord->year,['class'=> 'form-control form-control-user', 'placeholder'=> 'วันที่'])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('type','ภาคเรียน')}}
                        {{-- {!! Form::select('type', array('Mid' => 'กลางภาค',' End'=>'ปลายภาค'),$exam->type,['class'=> 'form-control form-control-user']) !!} --}}
                        <select class="custom-select custom-select-sm" name="term">
                            @foreach ([
                            '1' => 'กลางภาค','2'=>'ปลายภาค'] as
                            $key => $term)
                            <option value="{{ $key }}" @if ($key==old($schoolRecord->term,
                                $schoolRecord->term))
                                selected="selected"
                                @endif
                                >{{ $term }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <h1 class="h6 text-gray-900 mb-4">คะแนนเต็ม</h1>
                        {{Form::text('score_max',$schoolRecord->score_max,['class'=> 'form-control form-control-user', 'placeholder'=> 'คะแนนเต็ม', 'maxlength'=>"10"])}}
                    </div>
                    <div class="form-group">
                        <h1 class="h6 text-gray-900 mb-4">คะแนน</h1>
                        {{Form::text('score',$schoolRecord->score,['class'=> 'form-control form-control-user', 'placeholder'=> 'คะแนน', 'maxlength'=>"10"])}}
                    </div>
                    {{Form::hidden('_method','PUT')}}
                    {{Form::submit('บันทึก',['class'=>'btn btn-primary btn-user btn-block'])}}
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>

@endsection