@extends('layout.app')

@section('content')

<div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
        <div class="row">
            <div class="col-lg">
                <div class="p-5">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">เพิ่มข้อมูลคะแนนเก็บ</h1>
                    </div>

                    {!! Form::open(['action' => 'SchoolRecordController@store', 'method' => 'POST']) !!}
                    
                    {{-- <div class="form-group">
                        {{Form::label('date','วันที่:')}}
                        {{ Form::text('date', new DateTime(),['class'=> 'form-control form-control-user', 'placeholder'=> 'วันที่']) }}
                    </div> --}}
                    <div class="form-group">
                        {{Form::label('date','ปีการศึกษา')}}<span style="color:red">*</span>
                        {{Form::number('year','',['class'=> 'form-control form-control-user', 'placeholder'=> 'ปีการศึกษา', 'maxlength'=>"6"])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('type','ภาคเรียน')}}
                        <span style="color:red">*</span>
                        <select class="form-control form-control-user" name="term">
                            <option value='1'>กลางภาค</option>
                            <option value='2'>ปลายภาค</option>
                        </select>
                    </div>
                    <div class="form-group">
                        {{Form::label('subject_id','วิชา')}}<span style="color:red">*</span>
                        <select class="form-control form-control-user" name="subject_id">
                            @foreach($student->room->timetables as $timetable)
                            <option value={{$timetable->id}}>{{$timetable->subject->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        {{Form::label('date','คะแนนเต็ม')}}<span style="color:red">*</span>
                        {{Form::number('score_max','',['class'=> 'form-control form-control-user', 'placeholder'=> 'คะแนนเต็ม', 'maxlength'=>"10"])}}
                    </div>
                    <div class="form-group">
                        {{Form::label('date','คะแนน')}}<span style="color:red">*</span>
                        {{Form::number('score','',['class'=> 'form-control form-control-user', 'placeholder'=> 'คะแนน', 'maxlength'=>"10"])}}
                    </div>
                    <input name="student_id" type="hidden" value={{$student->id}}>
                    
                    {{Form::submit('บันทึก',['class'=>'btn btn-primary btn-user btn-block'])}}
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>

@endsection