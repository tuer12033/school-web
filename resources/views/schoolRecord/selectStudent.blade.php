@extends('layout.app')

@section('content')

<div class="container-fluid">
    <!-- DataTales Example -->

    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>รหัส</th>
                        <th>ชื่อ</th>
                        <th>นามสกุล</th>
                        <th>ห้อง</th>
                        <th>ผู้ปกครอง</th>
                        <th>เลือกนักเรียน</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($students as $student)

                    <tr>
                        <td>{{$student->code}}</td>
                        <td>{{$student->firstName}}</td>
                        <td>{{$student->lastName}}</td>
                        <td>{{$student->room->roomName}}</td>
                        <td>{{$student->parent->firstName}}
                            {{$student->parent->lastName}}
                        </td>
                        <td><a href="/schoolRecords/insertDetail/{{$student->id}}"
                                class="btn btn-secondary btn-icon-split">
                                <span class="text">เลือกนักเรียน</span>
                            </a>
                        </td>

                    </tr>

                    @endforeach

                </tbody>
            </table>
        </div>
    </div>

</div>

@endsection