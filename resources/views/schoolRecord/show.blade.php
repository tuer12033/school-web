@extends('layout.app')

@section('content')
<div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
        <div class="row">
            <div class="col-lg">
                <div class="p-5">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">ข้อมูลคะแนนเก็บ</h1>
                    </div>
                    <div class="form-group">
                        <h1 class="h6 text-gray-900 mb-4">นักเรียน</h1>
                        {{Form::text('classLevel',$schoolRecord->student->firstName.' '.$schoolRecord->student->lastName,['class'=> 'form-control form-control-user', 'readonly', 'placeholder'=> 'ระดับชั้น', 'maxlength'=>"50"])}}
                    </div>
                    <div class="form-group">
                        <h1 class="h6 text-gray-900 mb-4">วิชา</h1>
                        {{Form::text('subject',$schoolRecord->subject->name,['class'=> 'form-control form-control-user', 'readonly', 'placeholder'=> 'วิชา', 'maxlength'=>"50"])}}
                    </div>
                    <div class="form-group">
                        <h1 class="h6 text-gray-900 mb-4">ภาคเรียน</h1>
                        {{Form::text('date',$schoolRecord->year.'/'.$schoolRecord->term,['class'=> 'form-control form-control-user', 'readonly', 'placeholder'=> 'วันที่'])}}
                    </div>
                    <div class="form-group">
                        <h1 class="h6 text-gray-900 mb-4">คะแนนเต็ม</h1>
                        {{Form::text('time',$schoolRecord->score_max,['class'=> 'form-control form-control-user', 'readonly', 'placeholder'=> 'เวลา'])}}
                    </div>
                    <div class="form-group">
                        <h1 class="h6 text-gray-900 mb-4">คะแนน</h1>
                        {{Form::text('teacher',$schoolRecord->score,['class'=> 'form-control form-control-user', 'readonly', 'placeholder'=> 'ผู้คุมสอบ', 'maxlength'=>"50"])}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@endsection