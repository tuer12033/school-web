@extends('layout.app')

@section('content')

<div class="container-fluid">
    <!-- DataTales Example -->

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">ข้อมูลผู้ใช้</h6>
        </div>
        <a href="{{ route('register') }}" class="btn btn-primary">เพื่มข้อมูล</a>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>รหัส</th>
                            <th>ชื่อ</th>
                            <th>นามสกุล</th>
                            <th>ที่อยู่</th>
                            <th>เบอร์ติดต่อ</th>
                            <th>ตำแหน่ง</th>
                            <th>ข้อมูล</th>
                            <th>แก้ไข</th>
                            <th>ลบ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($teachers as $teacher)

                        <tr>
                            <td>{{$teacher->code}}</td>
                            <td>{{$teacher->firstName}}</td>
                            <td>{{$teacher->lastName}}</td>
                            <td>{{$teacher->address}}</td>
                            <td>{{$teacher->tel}}</td>
                            <td>{{$teacher->position}}</td>
                            <td><a href="/teachers/{{$teacher->id}}" class="btn btn-info btn-icon-split">
                                    <span class="text">ข้อมูล</span>
                                </a>
                            </td>
                            <td><a href="/teachers/{{$teacher->id}}/edit" class="btn btn-secondary btn-icon-split">
                                    <span class="text">แก้ไข</span>
                                </a>
                            </td>
                            <td>
                                {!! Form::open(['action' => ['TeacherController@destroy', $teacher->id], 'method' =>
                                'POST', 'class'=>'btn btn-danger btn-icon-split', 'onsubmit' => 'return confirm("ยืนยันต้องการลบข้อมูลหรือไม่ ?")']) !!}
                                {{Form::hidden('_method','DELETE')}}
                                {{Form::submit('ลบ',['class'=>'btn btn-danger'])}}
                                {!! Form::close() !!}
                            </td>
                        </tr>

                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

@endsection