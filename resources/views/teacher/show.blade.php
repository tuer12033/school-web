@extends('layout.app')

@section('content')
<div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
        <div class="row">
            <div class="col-lg">
                <div class="p-5">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">ข้อมูลผู้ใช้/คุณครู</h1>
                    </div>

                    <div class="form-group">
                        {{Form::text('code',$teacher->code,['class'=> 'form-control form-control-user', 'readonly', 'placeholder'=> 'รหัส', 'maxlength'=>"50"])}}
                    </div>
                    <div class="form-group row">

                        <div class="col-sm-6 mb-3 mb-sm-0">
                            {{Form::text('firstName',$teacher->firstName,['class'=> 'form-control form-control-user', 'readonly', 'placeholder'=> 'ชื่อ', 'maxlength'=>"50"])}}
                        </div>
                        <div class="col-sm-6">
                            {{Form::text('lastName',$teacher->lastName,['class'=> 'form-control form-control-user', 'readonly', 'placeholder'=> 'นามสกุล', 'maxlength'=>"50"])}}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::text('address',$teacher->address,['class'=> 'form-control form-control-user', 'readonly', 'placeholder'=> 'ที่อยู่'])}}
                    </div>
                    <div class="form-group">
                        {{Form::text('tel',$teacher->tel,['class'=> 'form-control form-control-user', 'readonly', 'placeholder'=> 'เบอร์ติดต่อ', 'maxlength'=>"20"])}}
                    </div>
                    <div class="form-group">
                        {{Form::text('position',$teacher->position,['class'=> 'form-control form-control-user', 'readonly', 'placeholder'=> 'ตำแหน่ง'])}}
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection