@extends('layout.app')

@section('content')

<div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
        <div class="row">
            <div class="col-lg">
                <div class="p-5">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">แก้ไขข้อมูลผู้ใช้</h1>
                    </div>
                    {!! Form::open(['action' => ['TeacherController@update', $teacher->id], 'method' => 'POST']) !!}
                    <div class="form-group">
                        {{Form::text('code',$teacher->code,['class'=> 'form-control form-control-user', 'placeholder'=> 'รหัส', 'maxlength'=>"50"])}}
                    </div>
                    <div class="form-group row">

                        <div class="col-sm-6 mb-3 mb-sm-0">
                            {{Form::text('firstName',$teacher->firstName,['class'=> 'form-control form-control-user', 'placeholder'=> 'ชื่อ', 'maxlength'=>"50"])}}
                        </div>
                        <div class="col-sm-6">
                            {{Form::text('lastName',$teacher->lastName,['class'=> 'form-control form-control-user', 'placeholder'=> 'นามสกุล', 'maxlength'=>"50"])}}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::text('address',$teacher->address,['class'=> 'form-control form-control-user', 'placeholder'=> 'ที่อยู่'])}}
                    </div>
                    <div class="form-group">
                        {{Form::text('tel',$teacher->tel,['class'=> 'form-control form-control-user', 'placeholder'=> 'เบอร์ติดต่อ', 'maxlength'=>"20"])}}
                    </div>
                    <div class="form-group">
                        <div class="form-group">
                            <h1 class="h6 text-gray-900 mb-4">เลือกตำแหน่ง</h1>
                            <select class="custom-select custom-select-sm" name="position">
                                <option value="Admin" @if ($teacher->position == 'Admin')
                                    selected="selected"
                                    @endif
                                    >แอดมิน</option>
                                <option value="Teacher" @if ($teacher->position == 'Teacher')
                                    selected="selected"
                                    @endif
                                    >อาจารย์</option>
                                <option value="Boss" @if ($teacher->position == 'Boss')
                                    selected="selected"
                                    @endif
                                    >ผู้อำนวยการ</option>
                            </select>
                        </div>
                    </div>
                    {{Form::hidden('_method','PUT')}}
                    {{Form::submit('บันทึก',['class'=>'btn btn-primary btn-user btn-block'])}}
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>

@endsection