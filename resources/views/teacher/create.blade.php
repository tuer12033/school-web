@extends('layout.app')

@section('content')

<div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
        <div class="row">
            <div class="col-lg">
                <div class="p-5">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">เพิ่มข้อมูลนักเรียน</h1>
                    </div>
                    {!! Form::open(['action' => 'StudentController@store', 'method' => 'POST']) !!}
                    <div class="form-group">
                        {{Form::label('date','รหัส')}}<span style="color:red">*</span>
                        {{Form::text('code','',['class'=> 'form-control form-control-user', 'placeholder'=> 'รหัส', 'maxlength'=>"50"])}}
                    </div>
                    <div class="form-group row">

                        <div class="col-sm-6 mb-3 mb-sm-0">
                            {{Form::label('date','ชื่อ')}}<span style="color:red">*</span>
                            {{Form::text('firstName','',['class'=> 'form-control form-control-user', 'placeholder'=> 'ชื่อ', 'maxlength'=>"50"])}}
                        </div>
                        <div class="col-sm-6">
                            {{Form::label('date','นามสกุล')}}<span style="color:red">*</span>
                            {{Form::text('lastName','',['class'=> 'form-control form-control-user', 'placeholder'=> 'นามสกุล', 'maxlength'=>"50"])}}

                        </div>
                    </div>
                    <div class="form-group">
                        <h1 class="h6 text-gray-900 mb-4">เลือกห้องเรียน<span style="color:red">*</span></h1>
                        <select class="custom-select custom-select-sm" name="room_id">
                            @foreach($rooms as $room)
                            <option value={{$room->id}}>{{$room->roomName}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <h1 class="h6 text-gray-900 mb-4">เลือกผู้ปกครอง<span style="color:red">*</span></h1>
                        <select class="custom-select custom-select-sm" name="parent_student_id">
                            @foreach($parents as $parent)
                            <option value={{$parent->id}}>{{$parent->firstName}} {{$parent->lastName}}</option>
                            @endforeach
                        </select>
                    </div>
                    {{Form::submit('บันทึก',['class'=>'btn btn-primary btn-user btn-block'])}}
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>

@endsection