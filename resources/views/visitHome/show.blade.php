@extends('layout.app')

@section('content')
<div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
        <div class="row">
            <div class="col-lg">
                <div class="p-5">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">ข้อมูลเยี่ยมบ้าน</h1>
                    </div>
                    <div class="form-group">
                        <h1 class="h6 text-gray-900 mb-4">บ้านนักเรียน</h1>
                        {{Form::text('classLevel',$visitHome->student->firstName.' '.$visitHome->student->lastName,['class'=> 'form-control form-control-user', 'readonly', 'placeholder'=> 'ระดับชั้น', 'maxlength'=>"50"])}}
                    </div>
                    <div class="form-group">
                        <h1 class="h6 text-gray-900 mb-4">ครู</h1>
                        {{Form::text('subject',$visitHome->teacher->firstName.' '.$visitHome->teacher->lastName,['class'=> 'form-control form-control-user', 'readonly', 'placeholder'=> 'วิชา', 'maxlength'=>"50"])}}
                    </div>
                    <div class="form-group">
                        <h1 class="h6 text-gray-900 mb-4">วันที่</h1>
                        {{Form::text('date',$visitHome->date,['class'=> 'form-control form-control-user', 'readonly', 'placeholder'=> 'วันที่'])}}
                    </div>
                    @if(count($images)>0)
                    <h1 class="h6 text-gray-900 mb-4">รูปภาพ</h1>
                    @foreach($images as $image)
                    <div class="form-group">
                        <img src='/images/visitHome/{{$image}}' height="300" width="300" class="css-class" alt="alt text">
                    </div>
                    @endforeach
                    @else
                    <h1 class="h6 text-gray-900 mb-4">ไม่มีรูปภาพ</h1>
                    @endif
                    {{-- <div class="form-group">
                        <h1 class="h6 text-gray-900 mb-4">เวลา</h1>
                        {{Form::text('time',$exam->start.' '.$exam->end,['class'=> 'form-control form-control-user', 'readonly', 'placeholder'=> 'เวลา'])}}
                    </div> --}}
                    {{-- <div class="form-group">
                        <h1 class="h6 text-gray-900 mb-4">ผู้คุมสอบ</h1>
                        {{Form::text('teacher',$exam->teacher->name,['class'=> 'form-control form-control-user', 'readonly', 'placeholder'=> 'ผู้คุมสอบ'])}}
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@endsection