@extends('layout.app')

@section('content')

<div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
        <div class="row">
            <div class="col-lg">
                <div class="p-5">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">เพิ่มข้อมูลเยี่ยมบ้าน</h1>
                    </div>

                    {!! Form::open(['action' => 'VisitHomeController@store', 'method' => 'POST', 'enctype'=>'multipart/form-data']) !!}
                    <div class="form-group">
                        {{Form::label('student_id','บ้านนักเรียน')}}
                        <span style="color:red">*</span>
                        <select class="form-control form-control-user" name="student_id">
                            @foreach($students as $student)
                            <option value={{$student->id}}>{{$student->firstName}} {{$student->lastName}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        {{Form::label('teacher_id','ครู')}}
                        <span style="color:red">*</span>
                        <select class="form-control form-control-user" name="teacher_id">
                            @foreach($teachers as $teacher)
                            <option value={{$teacher->id}}>{{$teacher->firstName}} {{$teacher->lastName}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        {{Form::label('date','วันที่')}}<span style="color:red">*</span>
                        {{ Form::date('date', new DateTime() ,['class'=> 'form-control form-control-user', 'placeholder'=> 'วันที่']) }}
                    </div>
                    
                    <div class="form-group">
                        <span style="color:red">*</span>
                        {{Form::label('type','เลือกรูปภาพ : ')}}
                        {{-- <select class="form-control form-control-user" name="type">
                            <option value='Mid'>กลางภาค</option>
                            <option value='End'>ปลายภาค</option>
                        </select> --}}
                        <input type="file" name="images[]" placeholder="address" multiple>

                    </div>
                    {{Form::submit('บันทึก',['class'=>'btn btn-primary btn-user btn-block'])}}
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>

@endsection