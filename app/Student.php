<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'students';
    public $primaryKey = 'id';
    public $timestamps = true;

    public function room(){
        return $this->belongsTo('App\Room');
    }
    public function parent(){
        return $this->belongsTo('App\ParentStudent', 'parent_student_id');
    }

    public function classLevel(){
        return $this->belongsTo('App\ClassLevel', 'classLevel_id');
    }
}
