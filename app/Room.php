<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $table = 'rooms';
    public $primaryKey = 'id';
    public $timestamps = true;

    public function students(){
        return $this->hasMany('App\Student')->where('isActive',true);
    }

    public function timetables(){
        return $this->hasMany('App\Timetable');
    }
}
