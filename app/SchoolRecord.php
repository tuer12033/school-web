<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolRecord extends Model
{
    protected $table = 'schoolRecords';
    public $primaryKey = 'id';
    public $timestamps = true;

    public function student(){
        return $this->belongsTo('App\Student','student_id');
    }
    
    public function subject(){
        return $this->belongsTo('App\Subject');
    }
}
