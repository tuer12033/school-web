<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitHome extends Model
{
    protected $table = 'visitHomes';
    public $primaryKey = 'id';
    public $timestamps = true;

    public function student(){
        return $this->belongsTo('App\Student')->where('isActive',true);
    }
    public function teacher(){
        return $this->belongsTo('App\User');
    }
}
