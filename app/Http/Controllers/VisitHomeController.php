<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File; 
use App\Exam;
use App\Subject;
use App\Student;
use App\User;
use App\ClassTime;
use App\ClassLevel;
use App\VisitHome;

class VisitHomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $visitHomes = VisitHome::all();
        return view('visitHome.index')->with('visitHomes', $visitHomes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $students = Student::all()->where('isActive', true);
        $teachers = User::all()->where('isActive', true);
        return view('visitHome.create')->with('students', $students)->with('teachers', $teachers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'student_id' => 'required',
            'teacher_id' => 'required',
            'date' => 'required',
            // 'images' => 'required',
            'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        $images = array();

        $visitHome = new VisitHome;
        $visitHome->student_id = $request->input('student_id');
        $visitHome->teacher_id = $request->input('teacher_id');
        $visitHome->date = $request->input('date');
        // $visitHome->images = $request->input('images');
        if ($files = $request->file('images')) {
            $i = 1;
            foreach ($files as $file) {
                $name = 'visit_studentId_' . $request->input('student_id') . '_image_' . $i . '.png';
                $file->move('images/visitHome', $name);
                $images[] = $name;
                $i++;
            }
            $visitHome->images = implode("|", $images);
        } else {
            $visitHome->images = null;
        }
        $visitHome->status = 'success';
        $visitHome->save();
        return redirect('/visitHomes')->with('success', 'บันทึกข้อมูลสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $visitHome = VisitHome::find($id);
        $images =[];
        if ($visitHome->images !== null) {
            $images = explode("|", $visitHome->images);
        }
        return view('visitHome.show')->with('visitHome', $visitHome)->with('images', $images);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $exam = Exam::find($id);
        $subjects = Subject::all()->where('isActive', true);
        $teachers = User::all()->where('isActive', true);
        $examTimes = ClassTime::all()->where('isActive', true);
        $classLevels = ClassLevel::all()->where('isActive', true);
        return view('visitHome.edit')->with('exam', $exam)->with('subjects', $subjects)->with('teachers', $teachers)->with('examTimes', $examTimes)->with('classLevels', $classLevels);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'student_id' => 'required',
            'teacher_id' => 'required',
            'date' => 'required',
            'images' => 'required',
        ]);

        $exam = Exam::find($id);
        $exam->classLevel_id = $request->input('classLevel_id');
        $exam->subject_id = $request->input('subject_id');
        $exam->teacher_id = $request->input('teacher_id');
        $exam->date = $request->input('date');
        $exam->start = $request->input('start');
        $exam->end = $request->input('end');
        $exam->type = $request->input('type');
        $exam->save();
        return redirect('/visitHomes')->with('success', 'แก้ไขข้อมูลสำเร็จ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $visitHome = VisitHome::find($id);
        $images =[];
        if ($visitHome->images !== null) {
            $images = explode("|", $visitHome->images);
        }
        foreach($images as $image){
            File::delete('images/visitHome/'.$image);
        }
        // $exam->isActive = false;
        // $exam->save();
        // $exam = Exam::find($id);
        $visitHome->delete();

        return redirect('/visitHomes')->with('success', 'ลบข้อมูลสำเร็จ');
    }
}
