<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class LoginController extends Controller
{
    // use AuthenticatesUsers;
    public function login($role)
    {
      return view('auth.login')->with('role', $role);
    }

    public function authenticate(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'position' => 'required|string',
        ]);

        $credentials = $request->only('email', 'password', 'position');

        if (Auth::attempt($credentials)) {
            return redirect()->intended('students');
        }

        return redirect()->route('login', ['role' => $request->position])->with('error', 'invalid email or password');
    }

    public function logout() {
      Auth::logout();

      return redirect('home');
    }

    public function home()
    {

      return view('students');
    }


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
