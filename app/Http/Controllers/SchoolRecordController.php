<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exam;
use App\Subject;
use App\User;
use App\ClassTime;
use App\ClassLevel;
use App\SchoolRecord;
use App\Student;
use App\Timetable;

class SchoolRecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $schoolRecords = SchoolRecord::all();
        return view('schoolRecord.index')->with('schoolRecords', $schoolRecords);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $students = Student::all()->where('isActive',true);
        // $classLevels = ClassLevel::all()->where('isActive',true);
        return view('schoolRecord.selectStudent')->with('students', $students);
    }

    public function insertDetail($students_id)
    {
        $student= Student::find($students_id);
        // $classLevels = ClassLevel::all()->where('isActive',true);
        return view('schoolRecord.create')->with('student', $student);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'student_id' => 'required',
            'year' => 'required',
            'term' => 'required',
            'subject_id' => 'required',
            'score_max' => 'required',
            'score' => 'required'
        ]);

        $schoolRecord = new SchoolRecord;
        $schoolRecord->student_id = $request->input('student_id');
        $schoolRecord->year = $request->input('year');
        $schoolRecord->term = $request->input('term');
        $schoolRecord->subject_id = $request->input('subject_id');
        $schoolRecord->score_max = $request->input('score_max');
        $schoolRecord->score = $request->input('score');
        $schoolRecord->save();
        return redirect('/schoolRecords')->with('success', 'บันทึกข้อมูลสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $schoolRecord = SchoolRecord::find($id);
        return view('schoolRecord.show')->with('schoolRecord',$schoolRecord);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $schoolRecord = SchoolRecord::find($id);
        $students = Student::all()->where('isActive',true);
        $subjects = Subject::all()->where('isActive',true);
        // $teachers = User::all()->where('isActive',true);
        $examTimes = ClassTime::all()->where('isActive',true);
        $classLevels = ClassLevel::all()->where('isActive',true);
        return view('schoolRecord.edit')->with('schoolRecord',$schoolRecord)->with('subjects', $subjects)->with('students', $students)->with('examTimes', $examTimes)->with('classLevels', $classLevels);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'student_id' => 'required',
            'year' => 'required',
            'term' => 'required',
            'subject_id' => 'required',
            'score_max' => 'required',
            'score' => 'required'
        ]);

        $schoolRecord = SchoolRecord::find($id);
        $schoolRecord->student_id = $request->input('student_id');
        $schoolRecord->year = $request->input('year');
        $schoolRecord->term = $request->input('term');
        $schoolRecord->subject_id = $request->input('subject_id');
        $schoolRecord->score_max = $request->input('score_max');
        $schoolRecord->score = $request->input('score');
        $schoolRecord->save();
        return redirect('/schoolRecords')->with('success', 'แก้ไขข้อมูลสำเร็จ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $exam = Exam::find($id);
        // $exam->isActive = false;
        // $exam->save();
        $schoolRecord = SchoolRecord::find($id);
        $schoolRecord->delete();

        return redirect('/schoolRecords')->with('success', 'ลบข้อมูลสำเร็จ');
    }
}
