<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ParentStudent;

class ParentStudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $parents = ParentStudent::all()->where('isActive',true);
        return view('parent.index')->with('parents', $parents);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('parent.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'firstName' => 'required',
            'lastName' => 'required',
            'address' => 'required',
            'tel' => 'required|max:10',
        ]);

        $parent = new ParentStudent;
        $parent->firstName = $request->input('firstName');
        $parent->lastName = $request->input('lastName');
        $parent->address = $request->input('address');
        $parent->tel = $request->input('tel');
        $parent->save();
        return redirect('/parents')->with('success', 'บันทึกข้อมูลสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $parent = ParentStudent::find($id);
        return view('parent.show')->with('students', $parent->students)->with('parent', $parent);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $parent = ParentStudent::find($id);
        return view('parent.edit')->with('parent',$parent);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'firstName' => 'required',
            'lastName' => 'required',
            'address' => 'required',
            'tel' => 'required',
        ]);

        $parent = ParentStudent::find($id);
        $parent->firstName = $request->input('firstName');
        $parent->lastName = $request->input('lastName');
        $parent->address = $request->input('address');
        $parent->tel = $request->input('tel');
        $parent->save();
        return redirect('/parents')->with('success', 'แก้ไขข้อมูลสำเร็จ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $parent = ParentStudent::find($id);
        $parent->isActive = false;
        $parent->save();
        // $parent = ParentStudent::find($id);
        // $parent->delete();

        return redirect('/parents')->with('success', 'ลบข้อมูลสำเร็จ');
    }
}
