<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exam;
use App\Subject;
use App\User;
use App\ClassTime;
use App\ClassLevel;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $exams = Exam::all()->where('isActive',true);;
        return view('exam.index')->with('exams', $exams);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subjects = Subject::all()->where('isActive',true);;
        $teachers = User::all()->where('isActive',true);;
        $examTimes = ClassTime::all()->where('isActive',true);;
        $classLevels = ClassLevel::all()->where('isActive',true);;
        return view('exam.create')->with('subjects', $subjects)->with('teachers', $teachers)->with('examTimes', $examTimes)->with('classLevels', $classLevels);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'classLevel_id' => 'required',
            'subject_id' => 'required',
            'teacher_id' => 'required',
            'date' => 'required',
            'start' => 'required',
            'end' => 'required',
            'type' => 'required',
        ]);

        $exam = new Exam;
        $exam->classLevel_id = $request->input('classLevel_id');
        $exam->subject_id = $request->input('subject_id');
        $exam->teacher_id = $request->input('teacher_id');
        $exam->date = $request->input('date');
        $exam->start = $request->input('start');
        $exam->end = $request->input('end');
        $exam->type = $request->input('type');
        $exam->save();
        return redirect('/exams')->with('success', 'บันทึกข้อมูลสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $exam = Exam::find($id);
        return view('exam.show')->with('exam',$exam);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $exam = Exam::find($id);
        $subjects = Subject::all()->where('isActive',true);
        $teachers = User::all()->where('isActive',true);
        $examTimes = ClassTime::all()->where('isActive',true);
        $classLevels = ClassLevel::all()->where('isActive',true);
        return view('exam.edit')->with('exam',$exam)->with('subjects', $subjects)->with('teachers', $teachers)->with('examTimes', $examTimes)->with('classLevels', $classLevels);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'classLevel_id' => 'required',
            'subject_id' => 'required',
            'teacher_id' => 'required',
            'date' => 'required',
            'start' => 'required',
            'end' => 'required',
            'type' => 'required',
        ]);

        $exam = Exam::find($id);
        $exam->classLevel_id = $request->input('classLevel_id');
        $exam->subject_id = $request->input('subject_id');
        $exam->teacher_id = $request->input('teacher_id');
        $exam->date = $request->input('date');
        $exam->start = $request->input('start');
        $exam->end = $request->input('end');
        $exam->type = $request->input('type');
        $exam->save();
        return redirect('/exams')->with('success', 'แก้ไขข้อมูลสำเร็จ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $exam = Exam::find($id);
        $exam->isActive = false;
        $exam->save();
        // $exam = Exam::find($id);
        // $exam->delete();

        return redirect('/exams')->with('success', 'ลบข้อมูลสำเร็จ');
    }
}
