<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Timetable;
use App\Room;
use App\Subject;
use App\User;
use App\ClassTime;

class TimetableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $rooms = Room::all()->where('isActive',true);;
        return view('timetable.index')->with('rooms', $rooms);
    }

    public function room($room_id)
    {
        $timetables = Timetable::where('room_id', $room_id)->where('isActive',true)->get();
        $room = Room::find($room_id);
        return view('timetable.roomDetail')->with('timetables', $timetables)->with('room', $room);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($room_id)
    {
        $room = Room::find($room_id);
        // $rooms = Room::all();
        $subjects = Subject::all()->where('isActive',true);
        $classTimes = ClassTime::all()->where('isActive',true);
        $teachers = User::all()->where('isActive',true);

        return view('timetable.create')->with('room', $room)->with('subjects', $subjects)->with('classTimes', $classTimes)->with('teachers', $teachers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'room_id' => 'required',
            'subject_id' => 'required',
            'teacher_id' => 'required',
            'class_time_id' => 'required'
        ]);

        $timetable = new Timetable;
        $timetable->room_id = $request->input('room_id');
        $timetable->subject_id = $request->input('subject_id');
        $timetable->teacher_id = $request->input('teacher_id');
        $timetable->class_time_id = $request->input('class_time_id');
        $timetable->save();
        return redirect('/timetables')->with('success', 'บันทึกข้อมูลสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $timetable = Timetable::find($id);
        return view('timetable.show')->with('timetable',$timetable);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $timetable = Timetable::find($id);
        $rooms = Room::all()->where('isActive',true);
        $subjects = Subject::all()->where('isActive',true);
        $classTimes = ClassTime::all()->where('isActive',true);
        $teachers = User::all()->where('isActive',true);
        return view('timetable.edit')->with('timetable',$timetable)->with('subjects', $subjects)->with('rooms', $rooms)->with('classTimes', $classTimes)->with('teachers', $teachers);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'room_id' => 'required',
            'subject_id' => 'required',
            'teacher_id' => 'required',
            'class_time_id' => 'required'
        ]);

        $timetable = Timetable::find($id);
        $timetable->room_id = $request->input('room_id');
        $timetable->subject_id = $request->input('subject_id');
        $timetable->teacher_id = $request->input('teacher_id');
        $timetable->class_time_id = $request->input('class_time_id');
        $timetable->save();
        return redirect('/timetables')->with('success', 'บันทึกข้อมูลสำเร็จ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $timetable = Timetable::find($id);
        $timetable->isActive = false;
        $timetable->save();
        // $timetable = Timetable::find($id);
        // $timetable->delete();

        return redirect('/timetables')->with('success', 'ลบข้อมูลสำเร็จ');
    }
}
