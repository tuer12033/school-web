<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subject;
use App\ClassLevel;


class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $subjects = Subject::all();
        return view('subject.index')->with('subjects', $subjects);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $classLevels = ClassLevel::all()->where('isActive',true);
        return view('subject.create')->with('classLevels', $classLevels);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'subjectCode' => 'required',
            'classLevel_id' => 'required'
        ]);

        $subject = new Subject;
        $subject->name = $request->input('name');
        $subject->subjectCode = $request->input('subjectCode');
        $subject->classLevel_id = $request->input('classLevel_id');
        $subject->save();
        return redirect('/subjects')->with('success', 'บันทึกข้อมูลสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $subject = Subject::find($id);
        return view('subject.show')->with('subject',$subject);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subject = Subject::find($id);
        $classLevels = ClassLevel::all()->where('isActive',true);
        return view('subject.edit')->with('subject',$subject)->with('classLevels', $classLevels);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'subjectCode' => 'required',
            'classLevel_id' => 'required'
        ]);

        $subject = Subject::find($id);
        $subject->name = $request->input('name');
        $subject->subjectCode = $request->input('subjectCode');
        $subject->classLevel_id = $request->input('classLevel_id');
        $subject->save();
        return redirect('/subjects')->with('success', 'แก้ไขข้อมูลสำเร็จ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subject = Subject::find($id);
        $subject->isActive = false;
        $subject->save();
        // $subject = Subject::find($id);
        // $subject->delete();

        return redirect('/subjects')->with('success', 'ลบข้อมูลสำเร็จ');
    }
}
