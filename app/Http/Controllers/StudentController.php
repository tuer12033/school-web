<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use App\Room;
use App\ParentStudent;
use App\ClassLevel;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::all()->where('isActive',true);
        return view('student.index')->with('students', $students);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rooms = Room::all()->where('isActive',true);
        $parents = ParentStudent::all()->where('isActive',true);
        $classLevels = ClassLevel::all()->where('isActive',true);
        return view('student.create')->with('rooms', $rooms)->with('parents', $parents)->with('classLevels', $classLevels);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'firstName' => 'required',
            'lastName' => 'required',
            'code' => 'required',
            'room_id' => 'required',
            'parent_student_id' => 'required',
            'classLevel_id' => 'required'
        ]);

        $student = new Student;
        $student->firstName = $request->input('firstName');
        $student->lastName = $request->input('lastName');
        $student->code = $request->input('code');
        $student->classLevel_id = $request->input('classLevel_id');
        $student->room_id = $request->input('room_id');
        $student->parent_student_id = $request->input('parent_student_id');
        $student->save();
        return redirect('/students')->with('success', 'บันทึกข้อมูลสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function show($id)
    {
        $student = Student::find($id);
        $rooms = Room::all();
        $parents = ParentStudent::all();
        return view('student.show')->with('student',$student)->with('rooms', $rooms)->with('parents', $parents);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = Student::find($id);
        $classLevels = ClassLevel::all()->where('isActive',true);
        $rooms = Room::all()->where('isActive',true);
        $parents = ParentStudent::all()->where('isActive',true);
        return view('student.edit')->with('student',$student)->with('rooms', $rooms)->with('parents', $parents)->with('classLevels', $classLevels);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'firstName' => 'required',
            'lastName' => 'required',
            'code' => 'required',
            'room_id' => 'required',
            'parent_student_id' => 'required',
            'classLevel_id' => 'required'
        ]);

        $student = Student::find($id);
        $student->firstName = $request->input('firstName');
        $student->lastName = $request->input('lastName');
        $student->code = $request->input('code');
        $student->classLevel_id = $request->input('classLevel_id');
        $student->room_id = $request->input('room_id');
        $student->parent_student_id = $request->input('parent_student_id');
        $student->save();
        return redirect('/students')->with('success', 'แก้ไขข้อมูลสำเร็จ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = Student::find($id);
        $student->isActive = false;
        $student->save();
        // $student = Student::find($id);
        // $student->delete();

        return redirect('/students')->with('success', 'ลบข้อมูลสำเร็จ');
    }
}
