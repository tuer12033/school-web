<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Room;
use App\Timetable;
use App\Student;
use App\CheckLineUp;

class CheckLineUpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $rooms = Room::all()->where('isActive',true);;
        return view('checkLineUp.selectRoom')->with('rooms', $rooms);
    }

    // public function selectTimetable($room_id)
    // {
    //     $timetables = Timetable::where('room_id', $room_id)->where('isActive',true)->get();
    //     return view('checkName.selectTimetable')->with('timetables', $timetables)->with('room_id', $room_id);
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function history()
    {
        $checkLineUps = CheckLineUp::all();
        return view('checkLineUp.history')->with('checkLineUps', $checkLineUps);
    }

    public function checking($room_id)
    {
        $students = Student::where('room_id', $room_id)->where('isActive',true)->get();
        return view('checkLineUp.checking')->with('students', $students);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'student_list' => 'required',
            'check_list' => 'required',
            'date' => 'required'
        ]);
        $students = $request->input('student_list');
        $status = $request->input('check_list');

        for($i=0;$i<count($students);$i++){
            $checkLineUp = new CheckLineUp;
            $checkLineUp->student_id = $students[$i];
            $checkLineUp->date = $request->input('date');
            $checkLineUp->status = $status[$students[$i]];
            $checkLineUp->save();
        }
        return redirect('/checkLineUps')->with('success', 'บันทึกข้อมูลสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
