<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Room;
use App\Timetable;
use App\Student;
use App\CheckName;

class CheckNameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $rooms = Room::all()->where('isActive',true);;
        return view('checkName.selectRoom')->with('rooms', $rooms);
    }

    public function selectTimetable($room_id)
    {
        $timetables = Timetable::where('room_id', $room_id)->where('isActive',true)->get();
        return view('checkName.selectTimetable')->with('timetables', $timetables)->with('room_id', $room_id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function history()
    {
        $checkNames = CheckName::all();
        return view('checkName.history')->with('checkNames', $checkNames);
    }

    public function checking($room_id, $timetable_id)
    {
        $students = Student::where('room_id', $room_id)->where('isActive',true)->get();
        return view('checkName.checking')->with('students', $students)->with('timetable_id', $timetable_id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'student_list' => 'required',
            'check_list' => 'required',
            'timetable_id' => 'required',
            'date' => 'required'
        ]);
        $students = $request->input('student_list');
        $status = $request->input('check_list');

        for($i=0;$i<count($students);$i++){
            $checkName = new CheckName;
            $checkName->student_id = $students[$i];
            $checkName->timetable_id = $request->input('timetable_id');
            $checkName->status = $status[$students[$i]];
            $checkName->date = $request->input('date');
            $checkName->save();
        }
        return redirect('/checkNames')->with('success', 'บันทึกข้อมูลสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
