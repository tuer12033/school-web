<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ClassTime;

class ClassTimeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $classTimes = ClassTime::all();
        return view('classTime.index')->with('classTimes', $classTimes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('classTime.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'number' => 'required',
            'date' => 'required',
            'start' => 'required',
            'end' => 'required'
        ]);

        $classTime = new ClassTime;
        $classTime->number = $request->input('number');
        $classTime->date = $request->input('date');
        $classTime->start = $request->input('start');
        $classTime->end = $request->input('end');
        $classTime->save();
        return redirect('/classTimes')->with('success', 'บันทึกข้อมูลสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $classTime = ClassTime::find($id)->where('isActive',true);;
        return view('classTime.show')->with('classTime',$classTime);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $classTime = ClassTime::find($id);
        return view('classTime.edit')->with('classTime',$classTime);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'number' => 'required',
            'date' => 'required',
            'start' => 'required',
            'end' => 'required'
        ]);

        $classTime = ClassTime::find($id);
        $classTime->number = $request->input('number');
        $classTime->date = $request->input('date');
        $classTime->start = $request->input('start');
        $classTime->end = $request->input('end');
        $classTime->save();
        return redirect('/classTimes')->with('success', 'แก้ไขข้อมูลสำเร็จ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $classTime = ClassTime::find($id);
        $classTime->isActive = false;
        $classTime->save();
        // $classTime = ClassTime::find($id);
        // $classTime->delete();

        return redirect('/classTimes')->with('success', 'ลบข้อมูลสำเร็จ');
    }
}
