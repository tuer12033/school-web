<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ClassLevel;

class ClassLevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $classLevels = ClassLevel::all()->where('isActive',true);
        return view('classLevel.index')->with('classLevels', $classLevels);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('classLevel.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $classLevel = new ClassLevel;
        $classLevel->name = $request->input('name');
        $classLevel->save();
        return redirect('/classLevels')->with('success', 'บันทึกข้อมูลสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $classLevel = ClassLevel::find($id);
        return view('classLevel.show')->with('classLevel', $classLevel);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $classLevel = ClassLevel::find($id);
        return view('classLevel.edit')->with('classLevel',$classLevel);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $classLevel = ClassLevel::find($id);
        $classLevel->name = $request->input('name');
        $classLevel->save();
        return redirect('/classLevels')->with('success', 'แก้ไขข้อมูลสำเร็จ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $classLevel = ClassLevel::find($id);
        $classLevel->isActive = false;
        $classLevel->save();
        // $classLevel = ClassLevel::find($id);
        // $classLevel->delete();

        return redirect('/classLevels')->with('success', 'ลบข้อมูลสำเร็จ');
    }
}
