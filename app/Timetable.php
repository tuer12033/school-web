<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Timetable extends Model
{
    protected $table = 'timetables';
    public $primaryKey = 'id';
    public $timestamps = true;

    public function room(){
        return $this->belongsTo('App\Room','room_id');
    }

    public function subject(){
        return $this->belongsTo('App\Subject');
    }

    public function classTime(){
        return $this->belongsTo('App\ClassTime');
    }

    public function checkNames(){
        return $this->hasMany('App\CheckName');
    }

    public function teacher(){
        return $this->belongsTo('App\User');
    }
}

