<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassTime extends Model
{
    protected $table = 'classTimes';
    public $primaryKey = 'id';
    public $timestamps = true;
}
