<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $table = 'subjects';
    public $primaryKey = 'id';
    public $timestamps = true;

    public function timetables(){
        return $this->hasMany('App\Exam');
    }
    public function classLevel(){
        return $this->belongsTo('App\ClassLevel', 'classLevel_id');
    }
}
