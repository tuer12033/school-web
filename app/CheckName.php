<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CheckName extends Model
{
    protected $table = 'checkNames';
    public $primaryKey = 'id';
    public $timestamps = true;

    public function timetable(){
        return $this->belongsTo('App\Timetable','timetable_id');
    }
    public function student(){
        return $this->belongsTo('App\Student','student_id');
    }
}
