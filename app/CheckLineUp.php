<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CheckLineUp extends Model
{
    protected $table = 'checkLineUps';
    public $primaryKey = 'id';
    public $timestamps = true;

    public function student(){
        return $this->belongsTo('App\Student','student_id');
    }
}
