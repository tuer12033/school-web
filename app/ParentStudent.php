<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParentStudent extends Model
{
    protected $table = 'parentStudents';
    public $primaryKey = 'id';
    public $timestamps = true;

    public function students(){
        return $this->hasMany('App\Student');
    }
}
