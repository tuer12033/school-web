<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    protected $table = 'exams';
    public $primaryKey = 'id';
    public $timestamps = true;

    public function subject(){
        return $this->belongsTo('App\Subject');
    }
    public function teacher(){
        return $this->belongsTo('App\User');
    }
    public function classLevel(){
        return $this->belongsTo('App\ClassLevel', 'classLevel_id');
    }
}
