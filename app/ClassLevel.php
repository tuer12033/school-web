<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassLevel extends Model
{
    protected $table = 'classLevels';
    public $primaryKey = 'id';
    public $timestamps = true;
    
    public function exams(){
        return $this->hasMany('App\Exam');
    }

    public function subjects(){
        return $this->hasMany('App\Subject');
    }

    public function students(){
        return $this->hasMany('App\Student');
    }
}
